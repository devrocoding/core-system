#!/bin/bash

name=\$1
host=\$2
port=\$3
server_type=\$4
min_ram=\$5
max_ram=\$6

new_server=/home/network/static-servers/\$name

sh /home/network/scripts/stop-static-server.sh \$name \$port

cp -r /home/network/server-template/plugins/. \$new_server/plugins/
cp -r /home/network/server-type-templates/\$server_type/. \$new_server/

cd \$new_server
tmux new-session -d -s \$name sh /home/network/scripts/run-jar-auto-restart.sh spigot.jar \$port \$min_ram \$max_ram  \$name \$server_type