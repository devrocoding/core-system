#!/bin/bash

echo "Killing existing instance..."

tmux kill-session -t Network

sleep 3

tmux new-session -d -s Network sh /home/network/scripts/run-jar-auto-restart.sh /home/network/core-system-0.1-SNAPSHOT-shadow.jar 0 256 512
