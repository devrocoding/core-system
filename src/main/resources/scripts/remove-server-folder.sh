#!/bin/bash

name=\$1

tmux kill-session -t \$name

rm -rf /home/network/servers/\$name