#!/bin/bash

name=\$1
port=\$2

fuser -k \$port/tcp
tmux kill-session -t \$name