#!/bin/bash

jar=\$1
port=\$2
min_ram=\$3
max_ram=\$4
name=\$5
server_type=\$6

while true
do
	java -Xms"\$min_ram"M -Xmx"\$max_ram"M -XX:+AlwaysPreTouch -Dname=\$name -Dgametype=\$server_type -XX:+DisableExplicitGC -XX:+UseG1GC -XX:+UnlockExperimentalVMOptions -XX:MaxGCPauseMillis=45 -XX:TargetSurvivorRatio=90 -XX:G1NewSizePercent=50 -XX:G1MaxNewSizePercent=80 -XX:InitiatingHeapOccupancyPercent=10 -XX:G1MixedGCLiveThresholdPercent=50 -XX:+AggressiveOpts -jar \$jar -o true -s 2000 -port \$port
	echo "Sleeping for 3 seconds"
	sleep 3
done