#!/bin/bash

# This script must be in static-servers/\$name/ to work

# Static Server Name:
name=TestHub
# RAM:
min_ram=512
max_ram=1024
# Address:
host=localhost
port=4000
# ServerType:
server_type=HUB

sh /home/network/scripts/start-static-server.sh \$name \$host \$port \$server_type \$min_ram \$max_ram
