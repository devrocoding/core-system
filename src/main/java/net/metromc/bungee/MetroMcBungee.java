package net.metromc.bungee;

import net.md_5.bungee.api.plugin.Plugin;
import net.metromc.bungee.redis.BungeeRedisHandler;
import net.metromc.bungee.server.ServerRegistration;
import net.metromc.bungee.user.UserCacheListener;
import net.metromc.services.MongoDBService;
import net.metromc.services.RedisService;

public class MetroMcBungee extends Plugin {

    private MongoDBService mongoDBService;
    private BungeeRedisHandler bungeeRedisHandler;

    @Override
    public void onEnable() {
        this.mongoDBService = new MongoDBService();

        this.bungeeRedisHandler = new BungeeRedisHandler(new RedisService(getDataFolder()), this);
        getProxy().getPluginManager().registerListener(this, new UserCacheListener(this));
        getProxy().getPluginManager().registerListener(this, new ServerRegistration());

        bungeeRedisHandler.subscribe();
    }

    @Override
    public void onDisable() {

    }

    public MongoDBService getMongoDBService() {
        return mongoDBService;
    }

    public BungeeRedisHandler getBungeeRedisHandler() {
        return bungeeRedisHandler;
    }
}
