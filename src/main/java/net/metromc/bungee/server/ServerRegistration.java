package net.metromc.bungee.server;

import java.net.InetSocketAddress;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.metromc.bungee.redis.RedisMessageEvent;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.spigot.util.D;

public class ServerRegistration implements Listener {

    @EventHandler
    public void onRedisRecieve(RedisMessageEvent e) {
        if (e.getChannel().equals(ServerDataRepository.REGISTER_SERVER_CHANNEL)) {
            String[] splitted = e.getMessage().split(" ");
            System.out.println(e.getMessage());
            registerServer(splitted[0], splitted[1], Integer.valueOf(splitted[2]));
        } else if (e.getChannel().equals(ServerDataRepository.UNREGISTER_SERVER_CHANNEL)) {
            unregisterServer(e.getMessage());
        }
    }

    private void registerServer(String name, String host, int port) {
        System.out.println("registering " + name + " " + host + ":" + port);
        ServerInfo serverInfo = ProxyServer.getInstance().constructServerInfo(name, new InetSocketAddress(host, port), "", false);
        ProxyServer.getInstance().getServers().put(name, serverInfo);
    }

    private void unregisterServer(String name) {
        ProxyServer.getInstance().getServers().remove(name);
    }
}
