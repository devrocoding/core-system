package net.metromc.bungee.user;

import com.google.common.base.Supplier;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.metromc.bungee.MetroMcBungee;
import org.bson.Document;

public class UserCacheListener implements Listener {

    private final MetroMcBungee plugin;

    public UserCacheListener(MetroMcBungee plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void on(PlayerDisconnectEvent e) {
        Supplier<String> userData = plugin.getBungeeRedisHandler().getData(e.getPlayer().getUniqueId().toString());

        if (userData.get() != null) {
            System.out.println("Found data");
            Document document = Document.parse(userData.get());

            new Thread(() -> {
                Document localDocument = new Document();
                localDocument.putAll(document);
                localDocument.remove("_id");

                System.out.println(e.getPlayer().getUniqueId().toString());

                plugin.getMongoDBService().getDatabase().getCollection("user").updateOne(new Document("uuid", document.get("uuid")), new Document("$set", localDocument));
                plugin.getBungeeRedisHandler().removeData(e.getPlayer().getUniqueId().toString());
            }).run();
        }
    }

}
