package net.metromc.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import net.metromc.services.redis.ConnectionData;

public class MongoDBService {

    private final MongoClient mongo;
    private final MongoDatabase mongoDatabase;

    private final String host;
    private final Integer port;
    private final String databaseName;
    private final String username;
    private final String password;

    public MongoDBService() {

        this.host = System.getProperty("dbhost");
        this.port = Integer.valueOf(System.getProperty("dbport"));
        this.databaseName = System.getProperty("dbname");
        this.username = System.getProperty("dbuser");
        this.password = System.getProperty("dbpassword");

        this.mongo = new MongoClient(new ServerAddress(host, port), Arrays.asList(MongoCredential.createCredential(username, databaseName, password.toCharArray())));
        this.mongoDatabase = mongo.getDatabase(databaseName);
    }

    public MongoClient getMongo() {
        return mongo;
    }

    public MongoDatabase getDatabase() {
        return mongoDatabase;
    }
}
