package net.metromc.services;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import net.metromc.services.redis.RedisModule;
import net.metromc.services.redis.RedisRepository;

public class RedisService {

    private final Set<RedisModule> redisModules = new HashSet<>();
    private final RedisRepository repository;


    public RedisService(File dataFolder) {
        this.repository = new RedisRepository(new File(dataFolder + File.separator + "redis.json"));
    }

    public void registerRedisModule(RedisModule module) {
        this.redisModules.add(module);
    }

    public RedisRepository getRepository() {
        return this.repository;
    }

}
