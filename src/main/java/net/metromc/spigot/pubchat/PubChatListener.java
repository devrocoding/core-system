package net.metromc.spigot.pubchat;

import net.metromc.spigot.redis.RedisMessageRecieveEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PubChatListener implements Listener{

    @EventHandler
    public void onRecieve(RedisMessageRecieveEvent e) {
        //TODO: Loop trough all channels
        String[] list = e.getMessage().split("//");
        String sub = null;
        if (list.length > 1 && list[1] != null)
            sub = list[1];

        new PubChatEvent(e.getChannel(), sub, e.getMessage());
    }

}
