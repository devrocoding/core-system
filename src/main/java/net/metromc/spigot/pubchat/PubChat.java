package net.metromc.spigot.pubchat;

import lombok.Getter;
import lombok.Setter;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.util.C;

public class PubChat {

    @Getter
    private String channel;
    private MetroMC plugin;
    @Getter @Setter
    private String prefix = C.PREFIX + "[" +
            (hasSubChannel() ? "//" + getSubChannel().toUpperCase() : getChannel().toUpperCase()) + "]";
    @Getter
    private String subChannel;

    public PubChat(MetroMC plugin, String channel, String sub){
        plugin.getRedisHandler().registerChannel(channel + (hasSubChannel() ? "//" + sub : ""));
        this.channel = channel;
        this.plugin = plugin;
        this.subChannel = sub;
    }

    public boolean send(String data){
        try {
            plugin.getRedisHandler().publish(
                    channel + (hasSubChannel() ? "//" + getSubChannel() : ""),
                    getPrefix() + " " + data);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    /**
     *
     * @param data = Message to be sent
     * @param rank = Rank to be sent to
     * @return true if message has been send.
     */
    public boolean send(String data, Rank rank){
        try {
            plugin.getRedisHandler().publish(
                    channel + (hasSubChannel() ? "//" + getSubChannel() : ""),
                    getPrefix() + " " + data);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean hasSubChannel(){
        return subChannel != null;
    }

}