package net.metromc.spigot.pubchat;

import lombok.Getter;
import net.metromc.spigot.util.MetroEvent;

public class PubChatEvent extends MetroEvent {

    @Getter
    private String channel;
    @Getter
    private String subChannel;
    @Getter
    private String message;

    public PubChatEvent(String channel, String subChannel, String data) {
        this.channel = channel;
        this.subChannel = subChannel;
        this.message = data;
    }

}
