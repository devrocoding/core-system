package net.metromc.spigot.pubchat;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;

public class PubSubChatManager extends Module{

    public PubSubChatManager(MetroMC plugin){
        super(plugin, "PubSubChatManager");
        registerListener(new PubChatListener());
    }

}
