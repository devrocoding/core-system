package net.metromc.spigot.punishment;

import lombok.Getter;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.punishment.command.CommandPunish;
import net.metromc.spigot.punishment.object.Punishment;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.C;
import net.metromc.spigot.util.Domain;
import net.metromc.spigot.util.UtilTime;
import org.bukkit.ChatColor;

public class PunishmentManager extends Module {

    @Getter
    private final PunishmentHandler punishmentHandler;

    public PunishmentManager(MetroMC plugin) {
        super(plugin, "Punishment Manager");

        punishmentHandler = new PunishmentHandler(plugin);

        registerListener(punishmentHandler);
        registerCommand(new CommandPunish(plugin));
    }

    public void punish(User punished, User punisher, Punishment punishment) {
        if (getPlugin().getNetworkManager().getServerRepository().isOnline(punished.getName())) {
            getPunishmentHandler().handlePunishment(punished.getUuid(), punishment, getMessage(punisher, punishment));
        }
    }

    public String getMessage(User punisher, Punishment punishment) {
        switch (punishment.getType()) {
            case BAN:
                return "\n" + ChatColor.RED + ChatColor.BOLD.toString() + "You have been banned" +
                        "\n" + ChatColor.WHITE + "By " + C.MESSAGE_HIGHLIGHT + punisher.getName() +
                        "\n" + ChatColor.WHITE + "For " + C.MESSAGE_HIGHLIGHT + (punishment.getExpireTime() == -1 ? "permanent" : UtilTime.formatTime(punishment.getIssuedTime() + punishment.getExpireTime() - System.currentTimeMillis())) +
                        "\n" + ChatColor.WHITE + "You can appeal at " + C.MESSAGE_HIGHLIGHT + Domain.getDomain().getWebsite();
            case MUTE:
                return C.PRIMARY_MESSAGE + "You are muted by " + C.MESSAGE_HIGHLIGHT + punisher.getName() + C.PRIMARY_MESSAGE + " for '" + C.MESSAGE_HIGHLIGHT + punishment.getReason() + C.PRIMARY_MESSAGE + "' till " + C.MESSAGE_HIGHLIGHT + (punishment.getExpireTime() == -1 ? "permanent" : UtilTime.formatTime(punishment.getIssuedTime() + punishment.getExpireTime() - System.currentTimeMillis()));
            case KICK:
                return "\n" + C.PRIMARY_MESSAGE + "You are kicked by " + C.MESSAGE_HIGHLIGHT + punisher.getName() + C.PRIMARY_MESSAGE + " for " + C.MESSAGE_HIGHLIGHT + punishment.getReason();
            case WARNING:
                return C.PRIMARY_MESSAGE + "You have been " + ChatColor.RED + ChatColor.BOLD + "WARNED" + C.PRIMARY_MESSAGE + " by " + C.MESSAGE_HIGHLIGHT + punisher.getName() + C.PRIMARY_MESSAGE + " for " + C.MESSAGE_HIGHLIGHT + punishment.getReason();
            default:
                return "";
        }
    }

}
