package net.metromc.spigot.punishment.object;

import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.D;
import org.bson.Document;

public class Punishment {

    @Getter
    private final UUID punisher;
    @Getter
    private final UUID punished;
    @Getter
    private final String reason;
    @Getter
    private final PunishmentIcon icon;
    @Getter
    private final PunishmentType type;
    @Getter
    private final long issuedTime;
    @Getter
    private final long expireTime;
    @Getter @Setter
    private boolean active;
    @Getter
    private UUID deactivator;

    public Punishment(UUID punisher, UUID punished, String reason, PunishmentIcon icon, PunishmentType type, long issuedTime, long expireTime, boolean active, UUID deactivator) {
        this.punisher = punisher;
        this.punished = punished;
        this.reason = reason;
        this.icon = icon;
        this.type = type;
        this.issuedTime = issuedTime;
        this.expireTime = expireTime;
        this.active = active;
        this.deactivator = deactivator;
    }

    public Punishment(UUID punisher, UUID punished, String reason, PunishmentIcon icon, PunishmentType type, long issuedTime, long expireTime, boolean active) {
        this(punisher, punished, reason, icon, type, issuedTime, expireTime, active, null);
    }


    public Document toDocument() {
        Document document = new Document();

        document.put("punisher", punisher.toString());
        document.put("punished", punished.toString());
        document.put("reason", reason);
        document.put("icon", icon.name());
        document.put("type", type.name());
        document.put("issued", issuedTime);
        document.put("expire", expireTime);
        document.put("active", active);
        if (deactivator != null) {
            document.put("deactivator", deactivator.toString());
        }

        return document;
    }

    public void deactivate(UUID deactivator) {
        this.active = false;
        this.deactivator = deactivator;
    }

    public static Punishment fromDocument(Document doc) {
        UUID deactivator = doc.containsKey("deactivator") ? UUID.fromString(doc.getString("deactivator")) : null;

        return new Punishment(UUID.fromString(doc.getString("punisher")), UUID.fromString(doc.getString("punished")), doc.getString("reason"), PunishmentIcon.valueOf(doc.getString("icon")),
                PunishmentType.valueOf(doc.getString("type")), doc.getLong("issued"), doc.getLong("expire"), doc.getBoolean("active"), deactivator);
    }
}
