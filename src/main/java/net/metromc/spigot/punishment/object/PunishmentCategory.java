package net.metromc.spigot.punishment.object;


import lombok.Getter;
import net.metromc.spigot.gui.Gui;
import net.metromc.spigot.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum PunishmentCategory {

    GENERAL("General", new ItemStack(Material.ANVIL), new Integer[] {21, 30, 39, 48}, 3, "Whenever a player violates one of the general rules"),
    CHAT("Chat", new ItemStack(Material.BOOK), new Integer[] {23, 32, 41, 50}, 5, "Whenever a player violates one of the chat rules"),
    HACKING("Hacking", new ItemStack(Material.DIAMOND_SWORD), new Integer[] {25, 34, 43, 52}, 7, "Whenever a player is hacking"),
    MISC("Misc", new ItemStack(Material.NETHER_STAR), new Integer[] {27}, 7, false, "Warnings"),
    PERM_PUNISHMENTS("Misc", new ItemStack(Material.NETHER_STAR), new Integer[] {36, 45}, 7, false, "Perm punishments");

    @Getter
    private final String name;
    private final ItemStack item;
    @Getter
    private final String[] desc;
    @Getter
    private final Integer[] slots;
    @Getter
    private final int pos;
    @Getter
    private final boolean displayIcon;

    PunishmentCategory(String name, ItemStack item, Integer[] slots, int pos, String... desc) {
        this(name, item, slots, pos, true, desc);
    }

    PunishmentCategory(String name, ItemStack item, Integer[] slots, int pos, boolean displayIcon, String... desc) {
        this.name = name;
        this.item = item;
        this.desc = desc;
        this.slots = slots;
        this.pos = pos;
        this.displayIcon = displayIcon;
    }

        public int getColumnIndex() {
        for (int i = 0; i < values().length; i++) {
            if (values()[i] == this)
                return i + 2;
        }
        return -1;
    }

    public int getFirstFreeSlot(Gui gui) {
        for (Integer i : getSlots()) {
            if (gui.getElement(i) == null) {
                return i;
            }
        }
        return -1;
    }

    public int getInventoryColumnIndex() {
        return 8 + (1 * getColumnIndex());
    }

    public ItemStack getItemStack() {
        ItemBuilder itemBuilder = new ItemBuilder(item.getType());
        itemBuilder.setName(ChatColor.RED + name);
        itemBuilder.addLore("");

        for (String s : getDesc()) {
            itemBuilder.addLore(ChatColor.WHITE + s);
        }

        itemBuilder.addItemFlags();

        return itemBuilder.build();
    }
}
