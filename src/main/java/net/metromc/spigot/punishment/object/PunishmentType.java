package net.metromc.spigot.punishment.object;

public enum PunishmentType {

    WARNING,
    KICK,
    MUTE,
    BAN,
    PERM_MUTE,
    PERM_BAN;
}
