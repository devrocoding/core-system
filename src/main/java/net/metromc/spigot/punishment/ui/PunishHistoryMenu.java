package net.metromc.spigot.punishment.ui;

import java.util.List;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.gui.Gui;
import net.metromc.spigot.gui.GuiElement;
import net.metromc.spigot.gui.context.GuiSize;
import net.metromc.spigot.punishment.object.Punishment;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.C;
import net.metromc.spigot.util.D;
import net.metromc.spigot.util.ItemBuilder;
import net.metromc.spigot.util.UtilString;
import net.metromc.spigot.util.UtilTime;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class PunishHistoryMenu extends Gui {

    public PunishHistoryMenu(MetroMC plugin, User owner) {
        super(plugin, owner.getName() + " - punish history", GuiSize.SIX_ROWS);

        for (int i = 0; i < GuiSize.SIX_ROWS.getSlots(); i++) {
        List<Punishment> punisments = owner.getPunishments();

        if (i >= punisments.size()) {
            continue;
        }

        Punishment punishment = punisments.get(i);

            addElement(new GuiElement() {
                @Override
                public ItemStack getIcon(Player player) {
                    ItemBuilder itemBuilder = new ItemBuilder(punishment.getIcon().getCategory().getItemStack().getType()).setName(punishment.getType().name());

                    itemBuilder.setLore(
                            ChatColor.GRAY + "Type: " + C.MESSAGE_HIGHLIGHT + UtilString.capitaliseFirstCharacter(punishment.getType().name()),
                            ChatColor.GRAY + "Punisher: " + C.MESSAGE_HIGHLIGHT + plugin.getUserManager().getUserCacheFirstDatabaseAfter(punishment.getPunisher()).getName(),
                            ChatColor.GRAY + "Reason: " + C.MESSAGE_HIGHLIGHT + punishment.getReason(),
                            ChatColor.GRAY + "Issued time: " + C.MESSAGE_HIGHLIGHT + UtilTime.getDate(punishment.getIssuedTime()),
                            ChatColor.GRAY + "Expire time: " + C.MESSAGE_HIGHLIGHT + UtilTime.getDate(punishment.getIssuedTime() + punishment.getExpireTime()),
                            ChatColor.GRAY + "Active: " + C.MESSAGE_HIGHLIGHT + punishment.isActive(),
                            (punishment.getDeactivator() == null ? "" : ChatColor.GRAY + "Deactivator: " + C.MESSAGE_HIGHLIGHT + plugin.getUserManager().getUserCacheFirstDatabaseAfter(punishment.getDeactivator()).getName()));
                    itemBuilder.hideEnchants();

                    ItemStack item = itemBuilder.build();

                    if (punishment.isActive()) {
                        item.addUnsafeEnchantment(Enchantment.DURABILITY, 2); //TODO fix glow...
                    }

                    return item;
                }

                @Override
                public void click(Player player, ClickType clickType) {
                    if (punishment.isActive()) {
                        punishment.deactivate(player.getUniqueId());
                        owner.updatePunishments(punisments);

                        if (plugin.getNetworkManager().getServerRepository().isOnline(owner.getName())) {
                            owner.update();
                        } else {
                            plugin.getUserManager().update(owner, false);
                        }

                        new PunishHistoryMenu(plugin, owner).open(player);
                    }


                }
            });
        }
    }
}
