package net.metromc.spigot.command.command;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.command.Command;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.C;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class CommandHelp extends Command {

    public CommandHelp(MetroMC plugin) {
        super(plugin, Rank.NONE, "Shows this gui", "help", "?");
    }

    @Override
    public void execute(Player player, String[] args) {
        User user = getPlugin().getUserManager().getUser(player);
        List<Command> commands = getPlugin().getCommandManager().getCommands();

        Comparator<Command> comparator = (a, b) -> a.getRank().ordinal() > b.getRank().ordinal() ? -1 : (a.getRank().ordinal() < b.getRank().ordinal() ? 1 : a.getAliases()[0].compareTo(b.getAliases()[0]));

        Collections.sort(commands, comparator);

        Rank rank = null;

        for (Command command : commands) {
            ChatColor color = command.getRank().getColor();

            if ((rank == null || !rank.equals(command.getRank())) && user.getRank().getId() >= command.getRank().getId()) {
                rank = command.getRank();
                player.sendMessage(C.PRIMARY_MESSAGE + "-= " + rank.getColor() + rank.name() + C.PRIMARY_MESSAGE + " =-");
            }

            if (user.getRank().getId() < command.getRank().getId()) {
                continue;
            }

            player.sendMessage(C.PRIMARY_MESSAGE + " /" + command.getAliases()[0] + " - " + color + command.getDescription());
        }
    }
}