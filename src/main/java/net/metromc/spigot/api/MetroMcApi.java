package net.metromc.spigot.api;

import lombok.Getter;
import net.metromc.spigot.MetroMC;

public class MetroMcApi extends MetroMC {

    @Getter
    private static MetroMcApi API;

    @Override
    public void enable() {
        API = this;
    }

    @Override
    public void disable() {

    }
}
