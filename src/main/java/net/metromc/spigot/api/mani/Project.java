package net.metromc.spigot.api.mani;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Project {

    /*
        Ik had een idee hierover maar ik weet niet meer wat.
        Komt nog wel.
     */

    String projectName()
            default "Project Name"; 

}
