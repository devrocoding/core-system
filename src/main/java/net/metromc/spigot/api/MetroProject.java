package net.metromc.spigot.api;

import net.metromc.spigot.MetroMC;

public abstract class MetroProject extends MetroMC {

    public abstract void init();
    public abstract void deInit();

    @Override
    public void enable() {
        init();
    }

    @Override
    public void disable() {
        deInit();
    }
}
