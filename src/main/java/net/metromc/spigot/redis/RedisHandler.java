package net.metromc.spigot.redis;

import net.metromc.services.RedisService;
import net.metromc.services.redis.MultiChannelRedisModule;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;

public class RedisHandler extends MultiChannelRedisModule {

    private final RedisService redisService;

    public RedisHandler(RedisService service) {
        super(service);

        this.redisService = service;
    }

    @Override
    public void recieve(String channel, String message) {
        Event event = new RedisMessageRecieveEvent(channel, message);

        Bukkit.getPluginManager().callEvent(event);
    }

    public RedisService getRedisService() {
        return redisService;
    }
}
