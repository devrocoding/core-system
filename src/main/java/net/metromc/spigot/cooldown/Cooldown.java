package net.metromc.spigot.cooldown;

import lombok.Getter;
import org.bson.Document;

public class Cooldown {

    @Getter
    private final String name;
    @Getter
    private final Long till;

    public Cooldown(String name, Long till) {
        this.name = name;
        this.till = till;
    }

    public Document toDocument() {
        Document document = new Document();

        document.put("name", name);
        document.put("till", till);

        return document;
    }

    public static Cooldown fromDocument(Document document) {
        return new Cooldown(document.getString("name"), document.getLong("till"));
    }
}
