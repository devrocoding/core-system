package net.metromc.spigot.cooldown.recharge;

import java.util.ArrayList;
import java.util.List;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.util.C;
import net.metromc.spigot.util.UtilMath;
import net.metromc.spigot.util.UtilPlayer;
import net.metromc.spigot.util.UtilTime;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Recharge {

    private static List<RechargeSession> rechargeSessions = new ArrayList<>();

    public static boolean recharge(MetroMC plugin, final Player p, final String element, double cooldown) {
        return recharge(plugin, p, element, cooldown, true, false);
    }

    public static boolean recharge(MetroMC plugin, final Player p, final String element, double cooldown, final boolean notify) {
        return recharge(plugin, p, element, cooldown, true, notify);
    }

    public static boolean recharge(MetroMC plugin, final Player p, final String element, double cooldown, boolean message, final boolean notify) {
        for (RechargeSession recharge : rechargeSessions) {
            if (recharge.getPlayer().equals(p) && recharge.getElement().equalsIgnoreCase(element)) {
                if (!UtilTime.elapsed(recharge.getTime(), (long) recharge.getCooldown() * 1000L)) {
                    if (recharge.getCooldown() > 1.0D && notify) {
                        double timeTill = ((recharge.getTime() + (recharge.getCooldown() * 1000L)) - System.currentTimeMillis()) / 1000.000D;

                        if (message) {
                            UtilPlayer.message(p, C.PRIMARY_MESSAGE + "You can't use " + C.MESSAGE_HIGHLIGHT + element + C.PRIMARY_MESSAGE + " for " + C.MESSAGE_HIGHLIGHT + UtilMath.formatDouble1DP(timeTill) + C.PRIMARY_MESSAGE + " seconds");
                        }

                    }
                    return false;
                } else {
                    rechargeSessions.remove(recharge);
                    return true;
                }
            }
        }

        final RechargeSession recharge = new RechargeSession(p, element, cooldown);
        rechargeSessions.add(recharge);

        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

            public void run() {
                rechargeSessions.remove(recharge);

            }
        }, (long) cooldown * 20);

        return true;
    }
}
