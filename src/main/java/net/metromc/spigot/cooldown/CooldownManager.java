package net.metromc.spigot.cooldown;

import com.google.common.collect.Lists;
import java.util.List;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.UtilTime;
import org.bson.Document;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class CooldownManager extends Module {

    public CooldownManager(MetroMC plugin) {
        super(plugin, "Cooldown Manager");
    }

    public Cooldown addCooldown(User user, Cooldown cooldown) {
        if (!user.getDocument().containsKey("cooldowns")) {
            user.put("cooldowns", Lists.newArrayList());
        }

        List<Document> documents = (List<Document>) user.getDocument().get("cooldowns");
        documents.add(cooldown.toDocument());

        user.put("cooldowns", documents);

        return cooldown;
    }

    public boolean hasCooldown(User user, String name, boolean message) {
        if(!user.getDocument().containsKey("cooldowns")) return false;

        List<Document> documents = (List<Document>) user.getDocument().get("cooldowns");

        for (int i = 0; i < documents.size(); i++) {
            Cooldown cooldown = Cooldown.fromDocument(documents.get(i));

            if(cooldown.getName().equalsIgnoreCase(name) && cooldown.getTill() > System.currentTimeMillis()) {
                user.getPlayer().sendMessage(ChatColor.RED + "You can use this again in " + ChatColor.stripColor(UtilTime.formatTime(cooldown.getTill() - System.currentTimeMillis())));
                return true;
            }

            if(cooldown.getTill() <= System.currentTimeMillis()){
                documents.remove(i);
                user.getDocument().put("cooldowns", documents);
            }
        }

        return false;
    }

    public boolean hasCooldown(Player player, String name, boolean message) {
        return hasCooldown(getPlugin().getUserManager().getUser(player), name, message);
    }
}
