package net.metromc.spigot.runnable;

import java.util.function.Consumer;
import net.metromc.spigot.MetroMC;
import org.bukkit.scheduler.BukkitRunnable;

public class MetroRunnable extends BukkitRunnable implements Cloneable {

    private final MetroMC plugin;
    private final RunnableManager runnableManager;
    private final String name;
    private final Consumer<MetroMC> run;

    public MetroRunnable(MetroMC plugin, RunnableManager runnableManager, String name, Consumer<MetroMC> run) {
        this.plugin = plugin;
        this.runnableManager = runnableManager;
        this.name = name;
        this.run = run;
    }

    @Override
    public void run() {
        run.accept(plugin);
    }

    public String getName() {
        return name;
    }

    @Override
    public synchronized void cancel() throws IllegalStateException {
        super.cancel();
        runnableManager.getRunnables().remove(name);
    }

    public Consumer<MetroMC> getRun() {
        return run;
    }

    public MetroRunnable clone() {
        try {
            return (MetroRunnable) super.clone();
        } catch (Exception ex) {
            return null;
        }
    }
}
