package net.metromc.spigot.npc;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.hologram.Hologram;
import net.metromc.spigot.util.UtilMath;
import net.minecraft.server.v1_12_R1.EntityLiving;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public abstract class NPC {

    private static final double NAMETAG_HEIGHT = 2.15D;
    public static final double SEND_RADIUS_SQUARED = UtilMath.square(40.0D);

    private final MetroMC plugin;
    private final Location location;
    private final List<String> name;
    private final Hologram hologram;

    private final List<UUID> viewers = Lists.newArrayList();

    private Consumer<Player> onClick;

    public NPC(MetroMC plugin, Location location, List<String> name) {
        this.plugin = plugin;
        this.location = location;
        this.name = name;

        Location tempLoction = location.clone().add(0, (name.size() == 1 ? name.size() : (name.size() - 1)) * NAMETAG_HEIGHT, 0);
        this.hologram = plugin.getHologramManager().createHologram(tempLoction, name);

    }

    public abstract void send(Player player);

    public abstract EntityLiving getEntity();

    public void sendAll() {
        Bukkit.getOnlinePlayers().forEach(player -> send(player));
    }

    public List<String> getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public Consumer<Player> onClick() {
        return onClick;
    }

    public void setOnClick(Consumer<Player> onClick) {
        this.onClick = onClick;
    }

    public MetroMC getPlugin() {
        return plugin;
    }

    public Hologram getHologram() {
        return hologram;
    }

    public List<UUID> getViewers() {
        return viewers;
    }

}
