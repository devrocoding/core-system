package net.metromc.spigot.npc;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import lombok.Getter;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.cooldown.recharge.Recharge;
import net.metromc.spigot.npc.npcs.EntityNPC;
import net.metromc.spigot.npc.npcs.HumanNPC;
import net.minecraft.server.v1_12_R1.EntityLiving;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class NPCManager extends Module {

    @Getter
    private final List<NPC> npcs = Lists.newArrayList();

    public NPCManager(MetroMC plugin) {
        super(plugin, "NPC Manager");

        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(plugin, ListenerPriority.NORMAL, PacketType.Play.Client.USE_ENTITY) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                if (event.getPacketType() == PacketType.Play.Client.USE_ENTITY) {
                    for (NPC npc : npcs) {
                        if (npc.getEntity().getId() == event.getPacket().getIntegers().read(0)) {
                            if (Recharge.recharge((MetroMC) plugin, event.getPlayer(), "NPC Click", 1.0D, false, false)) {
                                if (npc.onClick() != null) {
                                    npc.onClick().accept(event.getPlayer());
                                }
                            }
                        }
                    }
                }
            }
        });

        plugin.getRunnableManager().runTaskTimer("NPC Task", metroMC -> {
            Bukkit.getOnlinePlayers().forEach(o -> {
                npcs.forEach(npc -> npc.send(o));
            });
        }, 0, 20 * 2);
    }

    public EntityNPC createEntityNPC(Location location, Class<? extends EntityLiving> entityLivingClass, String... lines) {
        EntityNPC entityNPC = new EntityNPC(getPlugin(), location, Lists.newArrayList(lines), entityLivingClass);
        entityNPC.sendAll();

        npcs.add(entityNPC);

        return entityNPC;
    }

    public HumanNPC createHumanNPC(Location location, String texture, String signature, String... lines) {
        HumanNPC humanNPC = new HumanNPC(getPlugin(), location, Lists.newArrayList(lines), texture, signature);
        humanNPC.sendAll();

        npcs.add(humanNPC);

        return humanNPC;
    }

    public HumanNPC createHumanNPC(String name, Location location, String... lines) {
        try {
            URL uuidUrl = new URL("https://api.mojang.com/users/profiles/minecraft/" + name);
            InputStreamReader uuidReader = new InputStreamReader(uuidUrl.openStream());
            String uuid = new JsonParser().parse(uuidReader).getAsJsonObject().get("id").getAsString();

            URL textureUrl = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");
            InputStreamReader textureReader = new InputStreamReader(textureUrl.openStream());
            JsonObject textureProperty = new JsonParser().parse(textureReader).getAsJsonObject().get("properties").getAsJsonArray().get(0).getAsJsonObject();
            String texture = textureProperty.get("value").getAsString();
            String signature = textureProperty.get("signature").getAsString();

            HumanNPC humanNPC = new HumanNPC(getPlugin(), location, Lists.newArrayList(lines), texture, signature);
            humanNPC.sendAll();

            npcs.add(humanNPC);

            return humanNPC;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @EventHandler
    public void on(PlayerJoinEvent event) {
        npcs.forEach(npc -> npc.send(event.getPlayer()));
    }

    @EventHandler
    public void on(PlayerQuitEvent event) {
        npcs.forEach(npc -> {
            if (npc.getViewers().contains(event.getPlayer().getUniqueId())) {
                npc.getViewers().remove(event.getPlayer().getUniqueId());
            }
        });
    }


}
