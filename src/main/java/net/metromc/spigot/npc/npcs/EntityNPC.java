package net.metromc.spigot.npc.npcs;

import java.util.List;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.npc.NPC;
import net.metromc.spigot.util.D;
import net.metromc.spigot.util.UtilPlayer;
import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_12_R1.World;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.entity.Player;

public class EntityNPC extends NPC {

    private EntityLiving entityLiving;

    public EntityNPC(MetroMC plugin, Location location, List<String> name, Class<? extends EntityLiving> entityClass) {
        super(plugin, location, name);

        World world = ((CraftWorld) location.getWorld()).getHandle();

        try {
            this.entityLiving = entityClass.getConstructor(World.class).newInstance(world);
            this.entityLiving.spawnIn(world);
            this.entityLiving.yaw = location.getYaw();
            this.entityLiving.h(location.getYaw());
            this.entityLiving.setPositionRotation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void send(Player player) {
        if (!player.getWorld().equals(getLocation().getWorld())) {
            return;
        }

        if (!player.hasMetadata("NPC") && player.getLocation().distance(getLocation()) <= SEND_RADIUS_SQUARED) {
            if (!getViewers().contains(player.getUniqueId())) {
                UtilPlayer.sendPacket(player, new PacketPlayOutSpawnEntityLiving(entityLiving));
                UtilPlayer.sendPacket(player, new PacketPlayOutEntityHeadRotation(entityLiving, (byte) (getLocation().getYaw() * 256 / 360)));
                D.d("ADDED " + player.getName());
                getViewers().add(player.getUniqueId());
            }
        } else {
            if (getViewers().contains(player.getUniqueId())) {
                UtilPlayer.sendPacket(player, new PacketPlayOutEntityDestroy(entityLiving.getId()));
                D.d("REMOVED " + player.getName());
                getViewers().remove(player.getUniqueId());
            }
        }
    }

    @Override
    public EntityLiving getEntity() {
        return entityLiving;
    }
}
