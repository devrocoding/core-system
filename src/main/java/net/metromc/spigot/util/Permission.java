package net.metromc.spigot.util;

import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.user.object.User;

public class Permission {

    public static boolean allowed(User user, Rank rank) {
        return Permission.allowed(user, rank, true);
    }

    public static boolean allowed(User user, Rank rank, boolean notify) {
        if (user.getRank().getId() >= rank.getId()) {
            return true;
        }

        if (notify) {
            user.getPlayer().sendMessage(C.ERROR_COLOUR + "This action requires the rank " + rank.getPrefix());
        }
        return false;
    }
}
