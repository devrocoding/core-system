package net.metromc.spigot.util;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.api.MetroMcApi;
import net.metromc.spigot.user.object.Rank;
import org.bukkit.ChatColor;

public class D {

    public static void d(String message) {
        String msg = ChatColor.RED + ChatColor.BOLD.toString() + "DEBUG " + ChatColor.WHITE + message;
        MetroMC metroMC = MetroMcApi.getAPI();

        System.out.println(ChatColor.stripColor(msg));

        if (metroMC == null) {
            return;
        }

        MetroMcApi.getAPI().getUserManager().getUsers().forEach((uuid, user) -> {
            if (user == null) {
                return;
            }

            if (user.getRank().getId() >= Rank.ADMIN.getId()) {
                if (user == null && user.getPlayer() == null) { // final check maybe the user left
                    return;
                }
                user.getPlayer().sendMessage(msg);
            }
        });
    }
}