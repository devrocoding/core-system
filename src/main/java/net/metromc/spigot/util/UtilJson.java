package net.metromc.spigot.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializer;
import com.mongodb.util.JSONSerializers;

public class UtilJson {

    public static String getString(JsonObject jsonObject, String key) {
        return jsonObject.get(key).getAsString();
    }

    public static Integer getInt(JsonObject jsonObject, String key) {
        return jsonObject.get(key).getAsInt();
    }

    public static Short getShort(JsonObject jsonObject, String key) {
        return jsonObject.get(key).getAsShort();
    }

    public static Long getLong(JsonObject jsonObject, String key) {
        return jsonObject.get(key).getAsLong();
    }

    public static Boolean getBoolean(JsonObject jsonObject, String key) {
        return jsonObject.get(key).getAsBoolean();
    }
}
