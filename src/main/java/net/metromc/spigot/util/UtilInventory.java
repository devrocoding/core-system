package net.metromc.spigot.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

public class UtilInventory {

    public static Map getInventory(Inventory inv) {
        Map inventory = Maps.newHashMap();
        ItemStack[] contents = inv.getContents().clone();

        inv.clear();

        for (int i = 0; i < contents.length; i++) {
            ItemStack itemStack = contents[i];

            if (itemStack == null) {
                continue;
            }

            Map itemMap = Maps.newHashMap();
            String material = itemStack.getType().name();
            int amount = itemStack.getAmount();
            byte data = itemStack.getData().getData();
            short durability = itemStack.getDurability();

            itemMap.put("material", material);
            itemMap.put("amount", amount);
            itemMap.put("data", data);
            itemMap.put("durability", durability);

            if (itemStack.hasItemMeta()) {
                ItemMeta itemMeta = itemStack.getItemMeta();

                if (itemMeta.hasDisplayName()) {
                    itemMap.put("name", itemMeta.getDisplayName());
                }

                if (itemMeta.hasLore()) {
                    itemMap.put("lore", itemMeta.getLore());
                }

                if (itemMeta.isUnbreakable()) {
                    itemMap.put("unbreakable", true);
                }

                //TODO more metas
                if (itemMeta instanceof PotionMeta) {
                    PotionMeta potionMeta = (PotionMeta) itemMeta;

                    // base potion effect
                    PotionData potionData = potionMeta.getBasePotionData();

                    Map basePotionMap = Maps.newHashMap();
                    basePotionMap.put("type", potionData.getType().name());
                    basePotionMap.put("extended", potionData.isExtended());
                    basePotionMap.put("upgraded", potionData.isUpgraded());

                    itemMap.put("potion_effect",  basePotionMap);

                    // custom potion effects
                    List potionEffects = Lists.newArrayList();

                    for (PotionEffect potionEffect : potionMeta.getCustomEffects()) {
                        Map<String, Object> potionEffectMap = Maps.newHashMap();

                        potionEffectMap.put("type", potionEffect.getType().getName());
                        potionEffectMap.put("duration", potionEffect.getDuration());
                        potionEffectMap.put("amplifier", potionEffect.getAmplifier());

                        potionEffects.add(potionEffectMap);
                    }

                    itemMap.put("potion_effects", potionEffects);
                }

                if (itemMeta instanceof FireworkMeta) {
                    FireworkMeta fireworkMeta = (FireworkMeta) itemMeta;
                    Map baseFireworkMap = Maps.newHashMap();

                    baseFireworkMap.put("power", fireworkMeta.getPower());
                    itemMap.put("firework_effect", baseFireworkMap);

                    List fireworkEffects = Lists.newArrayList();

                    fireworkMeta.getEffects().forEach(fireworkEffect -> {
                        Map<String, Object> fireworkEffectMap = Maps.newHashMap();
                        Collection colors = Sets.newHashSet();
                        Collection fadeColors = Sets.newHashSet();

                        fireworkEffect.getColors().forEach(color -> colors.add(String.valueOf(color.asRGB())));
                        fireworkEffect.getFadeColors().forEach(color -> fadeColors.add(String.valueOf(color.asRGB())));

                        fireworkEffectMap.put("type", fireworkEffect.getType().name());
                        fireworkEffectMap.put("colors", colors);
                        fireworkEffectMap.put("fade_colors", fadeColors);

                        fireworkEffects.add(fireworkEffectMap);
                    });

                    itemMap.put("firework_effects", fireworkEffects);
                }

                if (itemMeta instanceof LeatherArmorMeta) {
                    Color color = ((LeatherArmorMeta) itemMeta).getColor();

                    itemMap.put("armor_meta", String.valueOf(color.asRGB()));
                }

                if (itemMeta instanceof BannerMeta) {
                    BannerMeta bannerMeta = (BannerMeta) itemMeta;

                    itemMap.put("banner_color", bannerMeta.getBaseColor().getColor().asRGB());

                    List patterns = Lists.newArrayList();
                    bannerMeta.getPatterns().forEach(pattern -> {
                        Map<String, Object> patternMap = Maps.newHashMap();

                        patternMap.put("pattern", pattern.getPattern().getIdentifier());
                        patternMap.put("color", pattern.getColor().getColor().asRGB());

                        patterns.add(patternMap);
                    });

                    itemMap.put("banner_patterns", patterns);
                }

                if (itemMeta instanceof BookMeta) {
                    BookMeta bookMeta = (BookMeta) itemMeta;
                    Map baseBookMap = Maps.newHashMap();

                    if (bookMeta.hasAuthor()) {
                        baseBookMap.put("author",  bookMeta.getAuthor());
                    }

                    if (bookMeta.hasTitle()) {
                        baseBookMap.put("title", bookMeta.getAuthor());
                    }

                    if (bookMeta.hasGeneration()) {
                        baseBookMap.put("generation", bookMeta.getGeneration().name());
                    }

                    if (bookMeta.hasPages()) {
                        baseBookMap.put("pages", bookMeta.getPages());
                    }

                    if (!baseBookMap.isEmpty()) {
                        itemMap.put("book_meta", baseBookMap);
                    }
                }

                if (itemMeta instanceof EnchantmentStorageMeta) {
                    EnchantmentStorageMeta enchantmentMeta = (EnchantmentStorageMeta) itemMeta;
                    List storedEnchants = Lists.newArrayList();

                    enchantmentMeta.getStoredEnchants().forEach((enchantment, level) -> {
                        Map enchantmentMap = Maps.newHashMap();

                        enchantmentMap.put("enchantment", enchantment.getName());
                        enchantmentMap.put("level",  String.valueOf(level));

                        storedEnchants.add(enchantmentMap);
                    });

                    itemMap.put("stored_enchants", storedEnchants);
                }

                if (itemMeta instanceof SkullMeta) {
                    SkullMeta skullMeta = (SkullMeta) itemMeta;

                    itemMap.put("skull_owner", skullMeta.getOwner());
                }

                net.minecraft.server.v1_12_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(itemStack);

                if (nmsItem.hasTag()) {
                    NBTTagCompound nbt = nmsItem.getTag();

                    if (nbt.hasKey("EntityTag")) {
                        if (nbt.getCompound("EntityTag").hasKey("id")) {
                            itemMap.put("spawn_egg", nbt.getCompound("EntityTag").getString("id"));
                        }
                    }
                }
            }

            if (!itemStack.getEnchantments().isEmpty()) {
                Map enchantments = Maps.newHashMap();

                for (Map.Entry<Enchantment, Integer> entry : itemStack.getEnchantments().entrySet()) {
                    enchantments.put(entry.getKey().getName(), entry.getValue());
                }

                itemMap.put("enchantments", enchantments);
            }

            inventory.put(String.valueOf(i),itemMap);
        }

        return inventory;
    }

    public static void fillInventory(Inventory inv, Map invData) {
        for (Object object : invData.entrySet()) {
            Map.Entry<String, Object> entry = (Map.Entry<String, Object>) object;

            int slot = Integer.parseInt(entry.getKey());
            Map itemMap = (Map) entry.getValue();

            Material material = Material.valueOf((String) itemMap.get("material"));
            int amount = (int) itemMap.get("amount");
            byte data = (byte) (int) itemMap.get("data");
            short durability = (short) (int) itemMap.get("durability");

            ItemStack itemStack = new ItemStack(material, amount);
            ItemMeta itemMeta = itemStack.getItemMeta();

            itemStack.setData(new MaterialData(material, data));
            itemStack.setDurability(durability);

            if (itemMap.containsKey("name")) {
                itemMeta.setDisplayName((String) itemMap.get("name"));
            }

            if (itemMap.containsKey("lore")) {
                itemMeta.setLore((List<String>) itemMap.get("lore"));
            }

            if (itemMap.containsKey("unbreakable")) {
                itemMeta.setUnbreakable((boolean) itemMap.get("unbreakable"));
            }

            if (itemMap.containsKey("potion_effect")) {
                PotionMeta potionMeta = (PotionMeta) itemMeta;

                Map basePotionMap = (Map) itemMap.get("potion_effect");
                PotionData potionData = new PotionData(PotionType.valueOf((String) basePotionMap.get("type")), (boolean) basePotionMap.get("extended"), (boolean) basePotionMap.get("upgraded"));
                potionMeta.setBasePotionData(potionData);

                if (itemMap.containsKey("potion_effects")) {
                    for (Object effect : (List<Object>) itemMap.get("potion_effects")) {
                        Map potionEffect = (Map) effect;

                        PotionEffectType type = PotionEffectType.getByName((String) potionEffect.get("type"));
                        int duration = (int) potionEffect.get("duration");
                        int amplifier = (int) potionEffect.get("amplifier");

                        potionMeta.addCustomEffect(new PotionEffect(type, duration, amplifier), true);
                    }
                }
            }

            if (itemMap.containsKey("firework_effect")) {
                FireworkMeta fireworkMeta = (FireworkMeta) itemMeta;

                Map baseFireworkMap = (Map) itemMap.get("firework_effect");
                fireworkMeta.setPower(Integer.valueOf((String) baseFireworkMap.get("power")));

                if (itemMap.containsKey("firework_effects")) {
                    for (Object effect : (List<Object>) itemMap.get("firework_effects")) {
                        Map fireworkEffect = (Map) effect;

                        List<String> stringcolors = (List<String>) fireworkEffect.get("colors");
                        List<String> stringfadeColors = (List<String>) fireworkEffect.get("fade_colors");
                        List<Color> colors = Lists.newArrayList();
                        List<Color> fadeColors = Lists.newArrayList();

                        stringcolors.forEach(string -> colors.add(Color.fromRGB(Integer.valueOf(string))));
                        stringfadeColors.forEach(string -> fadeColors.add(Color.fromRGB(Integer.valueOf(string))));

                        FireworkEffect.Type type = FireworkEffect.Type.valueOf((String) fireworkEffect.get("type"));

                        fireworkMeta.addEffect(FireworkEffect.builder().with(type).withColor(colors).withFade(fadeColors).build());
                    }
                }
            }

            if (itemMap.containsKey("armor_meta")) {
                LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemMeta;

                leatherArmorMeta.setColor(Color.fromRGB(Integer.valueOf(String.valueOf(itemMap.get("armor_meta")))));
            }

            if (itemMap.containsKey("banner_color")) {
                BannerMeta bannerMeta = (BannerMeta) itemMeta;

                bannerMeta.setBaseColor(DyeColor.getByColor(Color.fromRGB(Integer.valueOf(String.valueOf(itemMap.get("banner_color"))))));

                List<Pattern> patterns = Lists.newArrayList();
                for (Object object1 : (List<Object>) itemMap.get("banner_patterns")) {
                    Map pattern = (Map) object1;

                    Pattern pattern1 = new Pattern(DyeColor.getByColor(Color.fromRGB(Integer.valueOf((String) pattern.get("color")))), PatternType.valueOf((String) pattern.get("pattern")));
                    patterns.add(pattern1);
                }

                if (!patterns.isEmpty()) {
                    bannerMeta.setPatterns(patterns);
                }
            }

            if (itemMap.containsKey("book_meta")) {
                BookMeta bookMeta = (BookMeta) itemMeta;
                Map baseBookMap = (Map) itemMap.get("book_meta");

                if (baseBookMap.containsKey("author")) {
                    bookMeta.setAuthor((String) baseBookMap.get("author"));
                }

                if (baseBookMap.containsKey("generation")) {
                    bookMeta.setGeneration(BookMeta.Generation.valueOf((String) baseBookMap.get("generation")));
                }

                if (baseBookMap.containsKey("title")) {
                    bookMeta.setTitle((String) baseBookMap.get("title"));
                }

                if (baseBookMap.containsKey("pages")) {
                    bookMeta.setPages((List<String>) baseBookMap.get("pages"));
                }
            }

            if (itemMap.containsKey("stored_enchants")) {
                EnchantmentStorageMeta enchantmentMeta = (EnchantmentStorageMeta) itemMeta;

                List enchantments = (List) itemMap.get("stored_enchants");

                for (Object enchantment : enchantments) {
                    Map enchantmentMap = (Map) enchantment;

                    enchantmentMeta.addStoredEnchant(Enchantment.getByName((String) enchantmentMap.get("enchantment")), Integer.valueOf((String) enchantmentMap.get("level")), true); // ignoreLevelRestriction = true just to be safe
                }
            }

            if (itemMap.containsKey("skull_owner")) {
                SkullMeta skullMeta = (SkullMeta) itemMeta;

                skullMeta.setOwner((String) itemMap.get("skull_owner"));
            }

            itemStack.setItemMeta(itemMeta);

            if (itemMap.containsKey("spawn_egg")) {
                net.minecraft.server.v1_12_R1.ItemStack nmsItem = CraftItemStack.asNMSCopy(itemStack);
                nmsItem.setTag(new NBTTagCompound());

                NBTTagCompound entityTag = new NBTTagCompound();
                entityTag.setString("id", (String) itemMap.get("spawn_egg"));
                nmsItem.getTag().set("EntityTag", entityTag);

                itemStack = CraftItemStack.asBukkitCopy(nmsItem);
            }

            if (itemMap.containsKey("enchantments")) {
                Map<Enchantment, Integer> enchants = Maps.newHashMap();

                for (Object entr : ((Map) itemMap.get("enchantments")).entrySet()) {
                    Map.Entry<String, Object> ench = (Map.Entry<String, Object>) entr;

                    enchants.put(Enchantment.getByName(ench.getKey()), (Integer) ench.getValue());
                }
                itemStack.addUnsafeEnchantments(enchants);
            }

            inv.setItem(slot, itemStack);
        }
    }
}
