package net.metromc.spigot.discord;

import lombok.Getter;

import java.util.Base64;
import java.util.Random;

public class VerificationCode {

    @Getter
    private String primaryKey;
    private byte[] verification;

    public VerificationCode(String primaryKey){
        this.primaryKey = primaryKey;
    }

    public VerificationCode Generate(){
        this.verification = Base64.getEncoder().encode(this.primaryKey.getBytes());
        return this;
    }

    public String getVerification(){
        return new String(verification);
    }

    @Deprecated
    public VerificationCode generateRandom(){
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        this.primaryKey = salt.toString();
        return this;
    }

}
