package net.metromc.spigot.discord;

import lombok.Getter;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.MetroEvent;

import java.util.Base64;

public class DiscordbotVerifyEvent extends MetroEvent {

    @Getter
    private User user;
    @Getter
    private byte[] verificationCode;
    @Getter
    private String response;
    @Getter
    private String verificationCodeString;

    public DiscordbotVerifyEvent(User user, byte[] verificationCode, String response) {
        this.user = user;
        this.verificationCode = verificationCode;
        this.response = response;
        this.verificationCodeString = new String(Base64.getDecoder().decode(verificationCode));
    }

}
