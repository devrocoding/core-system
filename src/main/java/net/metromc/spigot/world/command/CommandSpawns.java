package net.metromc.spigot.world.command;

import com.google.common.collect.Lists;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.command.Command;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.util.C;
import net.metromc.spigot.world.WorldManager;
import net.metromc.spigot.world.object.SpawnPoint;
import net.metromc.spigot.world.object.WorldData;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class CommandSpawns extends Command {

    private final WorldManager worldManager;

    public CommandSpawns(MetroMC plugin, WorldManager worldManager) {
        super(plugin, Rank.ADMIN, "Spawn command", "spawns");

        this.worldManager = worldManager;

        setUsage("<create/delete/list/tp>");
    }

    @Override
    public void execute(Player player, String[] args) {
        if(checkArgs(args)) {
            Location playerLocation = player.getLocation();
            WorldData worldData = worldManager.getWorldData(player.getWorld());
            String spawnName = args.length >= 2 ? args[1].toUpperCase() : "";
            SpawnPoint spawnPoint = worldData.getSpawn(spawnName);

            if(args[0].equalsIgnoreCase("create") && args.length >= 2) {
                spawnPoint = new SpawnPoint(worldData, playerLocation.getBlockX(), playerLocation.getBlockY(), playerLocation.getBlockZ(), playerLocation.getYaw());

                worldData.getSpawns().putIfAbsent(spawnName, Lists.newArrayList());
                worldData.getSpawns().get(spawnName).add(spawnPoint);
                worldData.writeToFile();

                player.sendMessage(C.PRIMARY_MESSAGE + "You created the location '" + C.MESSAGE_HIGHLIGHT + spawnName + C.PRIMARY_MESSAGE + "'");
            } else if(args[0].equalsIgnoreCase("delete") && args.length >= 2) {
                if(spawnPoint != null) {
                    worldData.getSpawns().remove(spawnName);
                    worldData.writeToFile();

                    player.sendMessage(C.PRIMARY_MESSAGE + "You removed the location '" + C.MESSAGE_HIGHLIGHT + spawnName + C.PRIMARY_MESSAGE + "'");
                } else {
                    couldNotFind(player, "Spawn", spawnName);
                }
            } else if(args[0].equalsIgnoreCase("tp") && args.length >= 2) {
                if(spawnPoint != null) {
                    player.teleport(spawnPoint.getLocation(Bukkit.getWorld(worldData.getWorldName())).clone().add(0, 1, 0));
                    player.sendMessage(C.PRIMARY_MESSAGE + "You teleported to '" + C.MESSAGE_HIGHLIGHT + spawnName + C.PRIMARY_MESSAGE + "'");
                } else {
                    couldNotFind(player, "Spawn", spawnName);
                }
            } else if(args[0].equalsIgnoreCase("list")) {
                player.sendMessage(C.MESSAGE_HIGHLIGHT + "LOCATIONS: ");

                worldData.getSpawns().entrySet().forEach(entry -> {
                    String spawns = "";

                    for(SpawnPoint spPoint : entry.getValue()) {
                        spawns = spPoint.toStringFormatted() + " ";
                    }

                    player.sendMessage(ChatColor.WHITE + entry.getKey() + ChatColor.GRAY + " " + spawns.trim());
                });

            } else {
                player.sendMessage(getUsage() + " (name)");
            }
        } else {
            player.sendMessage(getUsage() + " (name)");
        }
    }
}
