package net.metromc.spigot.world.command;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.command.Command;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.util.C;
import net.metromc.spigot.util.Pair;
import net.metromc.spigot.world.WorldManager;
import net.metromc.spigot.world.object.LocationData;
import net.metromc.spigot.world.object.Region;
import net.metromc.spigot.world.object.WorldData;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class CommandRegion extends Command {

    private final WorldManager worldManager;

    public CommandRegion(MetroMC plugin, WorldManager worldManager) {
        super(plugin, Rank.ADMIN, "Create a region", "region", "rg");
        this.worldManager = worldManager;

        setUsage("<create/delete/list/wand>");
    }

    @Override
    public void execute(Player player, String[] args) {
        if(checkArgs(args)) {
            WorldData worldData = worldManager.getWorldData(player.getWorld());
            String regionName = args.length >= 2 ? args[1].toUpperCase() : "";
            Region region = worldData.getRegion(regionName);

            if(args[0].equalsIgnoreCase("create") && args.length >= 2) {
                if(!worldManager.getRegionListener().hasSelection(player)) {
                    player.sendMessage(C.ERROR_COLOUR + "You don't have a full selection.");
                } else if(region != null) {
                    couldNotFind(player, "Region", regionName);
                } else {
                    Pair<LocationData, LocationData> pair = worldManager.getRegionListener().getSelections().get(player.getUniqueId());
                    region = new Region(regionName, player.getWorld(), pair.getLeft(), pair.getRight());

                    worldData.getRegions().add(region);
                    worldData.writeToFile();

                    player.sendMessage(C.PRIMARY_MESSAGE + "You created the region " + C.MESSAGE_HIGHLIGHT + regionName);
                }
            } else if(args[0].equalsIgnoreCase("delete") && args.length >= 2) {
                if(region != null) {
                    worldData.getRegions().remove(region);
                    worldData.writeToFile();

                    player.sendMessage(C.PRIMARY_MESSAGE + "You deleted the region " + C.MESSAGE_HIGHLIGHT + regionName);
                } else {
                    couldNotFind(player, "Region", regionName);
                }
            } else if(args[0].equalsIgnoreCase("list")) {
                player.sendMessage(C.MESSAGE_HIGHLIGHT + "REGIONS: ");

                worldData.getRegions().forEach(rg -> {
                    player.sendMessage(ChatColor.GRAY + rg.toStringFormatted());
                });

            } else if(args[0].equalsIgnoreCase("wand")) {
                player.getInventory().addItem(worldManager.getRegionWand());
                player.sendMessage(ChatColor.GRAY + "Region selection wand. Left click point 1, right click point 2, then use /region create <name>.");

            } else {
                player.sendMessage(getUsage() + " (region)");
            }
        } else {
            player.sendMessage(getUsage() + " (region)");
        }
    }
}
