package net.metromc.spigot.world.listener;

import com.google.common.collect.Maps;
import java.util.Map;
import java.util.UUID;
import net.metromc.spigot.util.Pair;
import net.metromc.spigot.world.WorldManager;
import net.metromc.spigot.world.object.LocationData;
import net.metromc.spigot.world.object.Region;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class RegionListener implements Listener {

    private final WorldManager worldManager;
    private final Map<UUID, Pair<LocationData, LocationData>> selections = Maps.newHashMap();

    public RegionListener(WorldManager worldManager) {
        this.worldManager = worldManager;
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        if (p.getInventory().getItemInMainHand() != null && p.getInventory().getItemInMainHand().equals(this.worldManager.getRegionWand())) {
            if (e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                Block block = e.getClickedBlock();
                if (block != null) {
                    LocationData dataPoint = new LocationData(block.getWorld(), block.getX(), block.getY(), block.getZ());
                    if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
                        e.setCancelled(true);
                        this.setLeftSelection(p, dataPoint);
                    } else if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                        e.setCancelled(true);
                        this.setRightSelection(p, dataPoint);
                    }

                }
            }
        }
    }

    @EventHandler
    public void on(PlayerMoveEvent event) {
        new Thread(() -> {
            Region fromRegion = worldManager.getWorldData(event.getPlayer().getWorld()).getRegion(event.getFrom());
            Region toRegion = worldManager.getWorldData(event.getPlayer().getWorld()).getRegion(event.getTo());

            if(fromRegion != null) {
                if(toRegion == null) {
                    if(fromRegion.getLeave() != null) {
                        fromRegion.getLeave().accept(event.getPlayer());
                    }
                } else if(!fromRegion.getName().equals(toRegion.getName())) {
                    if(fromRegion.getLeave() != null) {
                        fromRegion.getLeave().accept(event.getPlayer());
                    }
                }
            }

            if(toRegion != null) {
                if(fromRegion == null) {
                    if(toRegion.getEnter() != null) {
                        toRegion.getEnter().accept(event.getPlayer());
                    }
                } else if(!toRegion.getName().equals(fromRegion.getName())) {
                    if(toRegion.getEnter() != null) {
                        toRegion.getEnter().accept(event.getPlayer());
                    }
                }
            }
        }).run();
    }

    public void setLeftSelection(Player p, LocationData locationData) {
        (this.selections.computeIfAbsent(p.getUniqueId(), (pl) -> {
            return new Pair<>();
        })).setLeft(locationData);

        p.sendMessage(ChatColor.GRAY + "Left point set to " + ChatColor.WHITE + locationData.toStringFormatted() + ChatColor.GRAY + ".");
    }

    public void setRightSelection(Player p, LocationData dataPoint) {
        (this.selections.computeIfAbsent(p.getUniqueId(), (pl) -> {
            return new Pair<>();
        })).setRight(dataPoint);

        p.sendMessage(ChatColor.GRAY + "Right point set to " + ChatColor.WHITE + dataPoint.toStringFormatted() + ChatColor.GRAY + ".");
    }

    public boolean hasSelection(Player p) {
        return this.selections.computeIfAbsent(p.getUniqueId(), (u) -> {
            return new Pair<>();
        }).isFull();
    }

    public Map<UUID, Pair<LocationData, LocationData>> getSelections() {
        return this.selections;
    }
}
