package net.metromc.spigot.world.object;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class SpawnPoint {

    private final WorldData worldData;
    private final int x;
    private final int y;
    private final int z;
    private final float yaw;

    public SpawnPoint(WorldData worldData, int x, int y, int z, float yaw) {
        this.worldData = worldData;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
    }

    public Location getLocation() {
        return this.getLocation(Bukkit.getWorlds().get(0));
    }

    public Location getLocation(String world) {
        return this.getLocation(Bukkit.getWorld(world));
    }

    public Location getLocation(World world) {
        Location location = world.getBlockAt(this.x, this.y, this.z).getLocation();

        location.setYaw(this.yaw);
        location.setX(Math.floor(location.getX()) + 0.5D);
        location.setZ(Math.floor(location.getZ()) + 0.5D);

        return location;
    }

    public String toString() {
        return this.x + "," + this.y + "," + this.z + "," + (int) this.yaw;
    }

    public String toStringFormatted() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ", " + (int) this.yaw + ")";
    }

    public static SpawnPoint fromString(WorldData worldData, String string) {
        if (string == null) {
            return null;
        }

        String[] splitted = string.split(",");

        if (splitted.length != 4) {
            return null;
        }

        try {
            return new SpawnPoint(worldData, Integer.parseInt(splitted[0]), Integer.parseInt(splitted[1]), Integer.parseInt(splitted[2]), Float.parseFloat(splitted[3]));
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public WorldData getWorldData() {
        return worldData;
    }
}
