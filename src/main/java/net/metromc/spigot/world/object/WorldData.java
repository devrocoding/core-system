package net.metromc.spigot.world.object;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

public class WorldData {

    private File file;
    private YamlConfiguration config;
    private String worldName;
    private final Map<String, List<SpawnPoint>> spawns = Maps.newHashMap();
    private final Map<String, LocationData> locationData = Maps.newHashMap();
    private final List<Region> regions = Lists.newArrayList();

    public WorldData(String worldName) {
        this.worldName = worldName;
        this.file = new File(worldName + File.separator + "map.yml");
        boolean exists = this.file.exists();

        if (!exists) {
            try {
                this.file.createNewFile();
            }catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        this.config = YamlConfiguration.loadConfiguration(file);
        if (exists) {
            this.readFile();
        } else {
            this.writeToFile();
        }
    }

    public void readFile() {
        if (this.config.getString("spawns").length() > 0) {
            for(String spawnString : Lists.newArrayList(Arrays.asList(this.config.getString("spawns").split("//")))) {
                String[] data = spawnString.split("%%");

                spawns.put(data[0], Lists.newArrayList());

                for(int i = 1; i < data.length; i++) {
                    this.spawns.get(data[0]).add(SpawnPoint.fromString(this, data[i]));
                }

            }
        }

        if (this.config.getString("locationData").length() > 0) {
            for(String dataString : Lists.newArrayList(Arrays.asList(this.config.getString("locationData").split("//")))) {
                String[] data = dataString.split("%%");
                this.locationData.put(data[0], LocationData.fromString(data[1]));
            }
        }

        if (this.config.getString("regions").length() > 0) {
            for(String regionString : Lists.newArrayList(Arrays.asList(this.config.getString("regions").split("//")))) {
                this.regions.add(Region.fromString(regionString));
            }
        }
    }

    public void writeToFile() {
        String spawnString = "";
        String locationData = "";
        String regions = "";

        for(Map.Entry<String, List<SpawnPoint>> spawnPoint : this.spawns.entrySet()) {
            String spawns = "";

            for(SpawnPoint spPoint : spawnPoint.getValue()) {
                spawns = spawns + "%%" + spPoint.toString();
            }

            spawnString = spawnString + spawnPoint.getKey() + spawns + " ";
        }

        for(Map.Entry<String, LocationData> locData : this.locationData.entrySet()) {
            locationData = locationData + locData.getKey() + "%%" + locData.getValue().toString() + " ";
        }

        for(Region region : this.regions) {
            regions = regions + region.toString() + " ";
        }

        this.config.set("spawns", spawnString.trim().replaceAll(" ", "//"));
        this.config.set("locationData", locationData.trim().replaceAll(" ", "//"));
        this.config.set("regions", regions.trim().replaceAll(" ", "//"));

        try {
            this.config.save(this.file);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public SpawnPoint getSpawn(String name) {
        return spawns.containsKey(name) ? spawns.get(name).get(0) : null;
    }

    public List<SpawnPoint> getSpawns(String name) {
        return spawns.containsKey(name) ? spawns.get(name) : Lists.newArrayList();
    }

    public LocationData getLocationData(String name) {
        return locationData.containsKey(name) ? locationData.get(name) : null;
    }

    public Region getRegion(String name) {
        return regions.stream().filter(region -> region.getName().equalsIgnoreCase(name.toUpperCase())).findAny().orElse(null);
    }

    public String getWorldName() {
        return worldName;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public Map<String, List<SpawnPoint>> getSpawns() {
        return spawns;
    }

    public Region getRegion(Location location) {
        return regions.stream().filter(region -> region.contains(location)).findAny().orElse(null);
    }



}
