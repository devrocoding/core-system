package net.metromc.spigot.user.listener;

import java.util.List;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.punishment.object.Punishment;
import net.metromc.spigot.punishment.object.PunishmentType;
import net.metromc.spigot.user.event.UserLoadEvent;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.C;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class UserListener implements Listener {

    private final MetroMC plugin;

    public UserListener(MetroMC plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void preJoin(AsyncPlayerPreLoginEvent e) {
        User user;
        com.google.common.base.Supplier<String> userData = plugin.getRedisHandler().getData(e.getUniqueId().toString());

        if (userData.get() == null) {
            user = plugin.getUserManager().getUser(e.getUniqueId(), true);

            if (user == null) {
                user = new User(plugin, e.getUniqueId(), e.getName(), e.getAddress().getHostAddress());
            } else {
                plugin.getUserManager().update(user, true);
            }

        } else {
            user = new User(plugin, Document.parse(userData.get()));
        }

        plugin.getUserManager().getUsers().put(e.getUniqueId(), user);

        List<Punishment> punishments = user.getPunishments();

        for (Punishment punishment : user.getPunishments()) {
            if (punishment.getType().equals(PunishmentType.BAN) || punishment.getType().equals(PunishmentType.PERM_BAN)) {
                if (punishment.getExpireTime() != -1 && punishment.getExpireTime() + punishment.getIssuedTime() <= System.currentTimeMillis()) {
                    punishment.setActive(false);
                    user.updatePunishments(punishments);
                    continue;
                }

                if (punishment.isActive()) {
                    e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, plugin.getPunishmentManager().getMessage(plugin.getUserManager().getUserCacheFirstDatabaseAfter(punishment.getPunisher()), punishment));
                    return;
                }
            }
        }

        user.put("name", e.getName());
        user.put("ip", e.getAddress().getHostAddress());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent e) {
        User user = plugin.getUserManager().getUser(e.getPlayer());

        if (user == null) {
            e.getPlayer().kickPlayer(C.ERROR_COLOUR + "Something went wrong while loading your profile!");
        }

        e.getPlayer().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100.0D);

        Bukkit.getPluginManager().callEvent(new UserLoadEvent(user));

        user.put("name", e.getPlayer().getName());
        user.update();
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        User user = plugin.getUserManager().getUser(e.getPlayer());

        Bukkit.getScheduler().runTaskLater(plugin, () -> plugin.getUserManager().getUsers().remove(user.getUuid()), 10);
    }
}
