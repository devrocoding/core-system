package net.metromc.spigot.user.command;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.command.Command;
import net.metromc.spigot.user.object.Rank;
import org.bukkit.entity.Player;

public class CommandTest extends Command {

    public CommandTest(MetroMC plugin) {
        super(plugin, Rank.DONATOR, "Test command", "test");
    }

    @Override
    public void execute(Player player, String[] args) {
        player.sendMessage("AYYYY");
    }
}
