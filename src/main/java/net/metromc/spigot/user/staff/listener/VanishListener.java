package net.metromc.spigot.user.staff.listener;

import com.google.common.collect.Sets;
import java.util.Set;
import java.util.UUID;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.user.settings.event.ToggleSettingEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class VanishListener implements Listener {

    private final Set<UUID> vanishedPlayers = Sets.newHashSet();
    private final MetroMC plugin;

    public VanishListener(MetroMC plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onToggle(ToggleSettingEvent e) {
        if (e.getSetting().equals("vanish")) {
            if (e.getValue()) {
                vanish(e.getUser());
            } else {
                unvanish(e.getUser());
            }
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        User user = plugin.getUserManager().getUser(e.getPlayer());

        boolean setting = user.getSetting(plugin.getSettingsManager().getSetting("vanish"));

        if (setting) {
            vanish(user);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        if (vanishedPlayers.contains(e.getPlayer().getUniqueId())) {
            vanishedPlayers.remove(e.getPlayer().getUniqueId());
        }
    }

    private void vanish(User user) {
        Set<Player> hiddenPlayers = Sets.newHashSet();

        plugin.getUserManager().getUsers().forEach((uuid, cur) -> {
            if (cur.getRank().getId() < Rank.ADMIN.getId()) {
                hiddenPlayers.add(cur.getPlayer());
            }
        });

        hiddenPlayers.forEach(player -> player.hidePlayer(user.getPlayer()));
        vanishedPlayers.add(user.getUuid());
    }

    private void unvanish(User user) {
        Bukkit.getOnlinePlayers().forEach(player -> player.showPlayer(user.getPlayer()));
        vanishedPlayers.remove(user.getUuid());
    }
}
