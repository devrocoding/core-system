package net.metromc.spigot.user.staff;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.user.staff.command.CommandVanish;
import net.metromc.spigot.user.staff.listener.VanishListener;

public class StaffManager extends Module {

    public StaffManager(MetroMC plugin) {
        super(plugin, "StaffManager");

        registerListener(new VanishListener(plugin));
        registerCommand(new CommandVanish(plugin));
    }


}
