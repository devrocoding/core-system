package net.metromc.spigot.user;

import com.google.common.collect.Maps;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.user.chat.ChatManager;
import net.metromc.spigot.user.command.CommandTest;
import net.metromc.spigot.user.listener.UserListener;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.user.staff.StaffManager;
import org.bson.Document;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class UserManager extends Module {

    private final ChatManager chatManager;
    private final StaffManager staffManager;
    private final Map<UUID, User> users = Maps.newHashMap();

    public UserManager(MetroMC plugin) {
        super(plugin, "User Manager");

        this.chatManager = new ChatManager(plugin);
        this.staffManager = new StaffManager(plugin);

        registerListener(new UserListener(plugin));
        registerCommand(new CommandTest(plugin));
    }

    public User getUser(Player player) {
        return getUser(player.getUniqueId(), false);
    }

    public User getUser(String name, Boolean database) {
        for (User user : users.values()) {
            if (user.getName().toLowerCase().equals(name.toLowerCase())) {
                return user;
            }
        }

        if (database) {
            FindIterable<Document> result = getCollection().find(new Document("name", name));

            if (result.first() != null) {
                User user = new User(getPlugin(), result.first());

                return user;
            }
        }

        return null;    }

    public User getUser(Player player, Boolean database) {
        return getUser(player.getUniqueId(), database);
    }

    public User getUser(UUID uuid, Boolean database) {
        if (users.containsKey(uuid)) {
            return users.get(uuid);
        }

        if (database) {
            FindIterable<Document> result = getCollection().find(new Document("uuid", uuid.toString()));
            if (result.first() != null) {
                return new User(getPlugin(), result.first());
            } else {
                return users.get(uuid);
            }
        }

        return null;
    }

    public User getUserCacheFirstDatabaseAfter(UUID uuid) {
            if (users.containsKey(uuid)) {
                return users.get(uuid);
            }

            com.google.common.base.Supplier<String> userData = getPlugin().getRedisHandler().getData(uuid.toString());

            if (userData != null) {
                Document document = Document.parse(userData.get());
                return new User(getPlugin(), document);
            }

        return getUser(uuid, true);
    }

    public void update(User user, Boolean redis) {
        if (redis) {
            getPlugin().getRedisHandler().setData(user.getUuid().toString(), user.getDocument().toJson());
        } else {
            runTaskAsync(() -> {
                Document localDocument = new Document();

                localDocument.putAll(user.getDocument());
                localDocument.remove("_id");

                getCollection().updateOne(new Document("uuid", user.getUuid()), new Document("$set", localDocument));
            });
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        e.setQuitMessage(null);
    }

    public MongoCollection<Document> getCollection() {
        return this.getPlugin().getDatabaseManager().getDatabase().getCollection("user");
    }

    public Map<UUID, User> getUsers() {
        return users;
    }

    public ChatManager getChatManager() {
        return chatManager;
    }

    public StaffManager getStaffManager() {
        return staffManager;
    }
}
