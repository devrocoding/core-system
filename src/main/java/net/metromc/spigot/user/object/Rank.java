package net.metromc.spigot.user.object;

import org.bukkit.ChatColor;

public enum Rank {
    NONE(0, "NONE", "", ChatColor.GRAY),
    DONATOR(1, "DONATOR", "DONATOR", ChatColor.YELLOW),
    HELPER(2, "HELPER", "HELPER", ChatColor.BLUE),
    ADMIN(3, "ADMIN", "ADMIN", ChatColor.RED),
    OWNER(4, "OWNER", "OWNER", ChatColor.DARK_RED);

    private final Integer id;
    private final String codeName;
    private final String prefix;
    private final ChatColor color;

    Rank(Integer id, String codeName, String prefix, ChatColor color) {
        this.id = id;

        this.codeName = codeName;
        this.prefix = prefix;
        this.color = color;
    }

    public static Rank fromName(String codeName) {
        for (Rank rank : values()) {
            if (rank.getCodeName().equals(codeName)) {
                return rank;
            }
        }
        return null;
    }

    public static Rank getFirstStaffRank() {
        return ADMIN;
    }

    public static Rank getFirstDonatorRank() {
        return DONATOR;
    }

    public ChatColor getTextColor() {
        if (this.getId() < Rank.ADMIN.getId()) {
            return ChatColor.GRAY;
        }

        return ChatColor.WHITE;
    }

    public Integer getId() {
        return id;
    }

    public String getCodeName() {
        return codeName;
    }

    public String getPrefix() {
        if (this.getId() >= Rank.getFirstDonatorRank().getId()) {
            return color + ChatColor.BOLD.toString() + prefix + ChatColor.RESET;
        }


        if (id == Rank.NONE.getId()) {
            return color.toString();
        }

        return color + prefix + (id >= Rank.getFirstDonatorRank().getId() ? ChatColor.RESET : null);
    }

    public ChatColor getColor() {
        return color;
    }
}
