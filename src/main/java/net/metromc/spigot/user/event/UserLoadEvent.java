package net.metromc.spigot.user.event;

import net.metromc.spigot.user.object.User;
import net.metromc.spigot.util.MetroEvent;

public class UserLoadEvent extends MetroEvent {

    private final User user;

    public UserLoadEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
