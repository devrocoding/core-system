package net.metromc.spigot.user.settings.event;

import net.metromc.spigot.user.object.User;
import net.metromc.spigot.user.settings.Setting;
import net.metromc.spigot.util.MetroEvent;

public class ToggleSettingEvent extends MetroEvent {

    private final User user;
    private final Setting setting;
    private final boolean value;

    public ToggleSettingEvent(User user, Setting setting, boolean value) {
        this.user = user;
        this.setting = setting;
        this.value = value;
    }

    public User getUser() {
        return user;
    }

    public Setting getSetting() {
        return setting;
    }

    public boolean getValue() {
        return value;
    }
}
