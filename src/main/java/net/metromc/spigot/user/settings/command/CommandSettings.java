package net.metromc.spigot.user.settings.command;

import net.metromc.spigot.MetroMC;
import net.metromc.spigot.command.Command;
import net.metromc.spigot.user.settings.ui.SettingsMenu;
import org.bukkit.entity.Player;

public class CommandSettings extends Command {

    public CommandSettings(MetroMC plugin) {
        super(plugin, "Open the settings menu", "settings", "setting");
    }

    @Override
    public void execute(Player player, String[] args) {
        new SettingsMenu(getPlugin()).open(player);
    }
}
