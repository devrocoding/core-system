package net.metromc.spigot.user.settings;

import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.user.object.Rank;
import net.metromc.spigot.user.object.User;
import net.metromc.spigot.user.settings.command.CommandSettings;
import org.bukkit.Material;

public class SettingsManager extends Module {

    private final Set<Setting> settings = Sets.newHashSet();

    public SettingsManager(MetroMC plugin) {
        super(plugin, "Settings Manager");

        registerCommand(new CommandSettings(plugin));

        addSetting(new Setting("Vanish", Rank.getFirstDonatorRank(), Material.EYE_OF_ENDER, false));
    }

    private void addSetting(Setting setting) {
        settings.add(setting);
    }

    public Setting getSettingsFromDatabaseString(String string) {
        return settings.stream().filter(setting -> setting.getDatabaseString().equals(string.toUpperCase())).findAny().orElse(null);
    }

    public List<Setting> getSettings(User user) {
        return settings.stream().filter(setting -> setting.getRank().getId() <= user.getRank().getId()).collect(Collectors.toList());
    }

    public Setting getSetting(String name) {
        return settings.stream().filter(setting -> setting.equals(name)).findFirst().orElse(null);
    }

    public Collection<Setting> getSettings() {
        return settings;
    }
}
