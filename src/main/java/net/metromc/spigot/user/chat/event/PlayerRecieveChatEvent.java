package net.metromc.spigot.user.chat.event;

import net.metromc.spigot.util.MetroEvent;
import org.bukkit.entity.Player;

public class PlayerRecieveChatEvent extends MetroEvent {

    private boolean cancelled;
    private Player player;
    private String prefix;
    private String msg;
    private PlayerRecieveChatEvent.MessageType type;
    private Player sender;

    public PlayerRecieveChatEvent(Player p, String msg, PlayerRecieveChatEvent.MessageType type, Player sender) {
        this.player = p;
        this.msg = msg;
        this.cancelled = false;
        this.type = type;
        this.sender = sender;
    }

    public Player getPlayer() {
        return player;
    }

    public Player getSender() {
        return sender;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getMsg() {
        return msg;
    }

    public PlayerRecieveChatEvent.MessageType getMessageType() {
        return type;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public static enum MessageType {

        PRIVATE_MESSAGE(true),
        PLAYER_MESSAGE(true),
        SERVER_MESSAGE(false),
        BUNGEE_BROADCAST(false),
        STAFF_NOTIFICATION(false);

        private boolean cancellable;

        private MessageType(boolean cancellable) {
            this.cancellable = cancellable;
        }

        public boolean isCancellable() {
            return cancellable;
        }

    }
}
