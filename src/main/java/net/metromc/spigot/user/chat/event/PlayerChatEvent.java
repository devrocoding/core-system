package net.metromc.spigot.user.chat.event;

import java.util.HashMap;
import java.util.UUID;
import net.metromc.spigot.util.MetroEvent;
import org.bukkit.entity.Player;

public class PlayerChatEvent extends MetroEvent {

    private boolean cancelled;
    private String cancelReason;
    private Player player;
    private String prefix = "";
    private String suffix = "";
    private HashMap<UUID, String> level = new HashMap<UUID, String>();
    private String msg;

    public PlayerChatEvent(Player p, String msg) {
        this.player = p;
        this.msg = msg;
        this.cancelled = false;
    }

    public Player getPlayer() {
        return player;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setLevelPrefix(String level) {
        this.level.put(player.getUniqueId(), level);
    }

    public String getLevelPrefix() {
        if (this.level.containsKey(player.getUniqueId()))
            return this.level.get(player.getUniqueId());
        return null;
    }

    public String getMsg() {
        return msg;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled, String cancelReason) {
        this.cancelled = cancelled;
        this.cancelReason = cancelReason;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
        this.cancelReason = null;
    }
}
