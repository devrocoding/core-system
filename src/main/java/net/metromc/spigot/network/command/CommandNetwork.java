package net.metromc.spigot.network.command;


import net.metromc.spigot.MetroMC;
import net.metromc.spigot.command.Command;
import net.metromc.spigot.network.ui.NetworkGui;
import net.metromc.spigot.user.object.Rank;
import org.bukkit.entity.Player;

public class CommandNetwork extends Command {

    public CommandNetwork(MetroMC plugin) {
        super(plugin, Rank.ADMIN, "Open the server gui", "network");
    }

    @Override
    public void execute(Player player, String[] args) {
        new NetworkGui(getPlugin()).open(player);
    }
}
