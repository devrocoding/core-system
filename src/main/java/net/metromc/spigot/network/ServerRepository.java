package net.metromc.spigot.network;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Getter;
import net.metromc.network.server.data.Server;
import net.metromc.network.server.data.ServerType;
import net.metromc.network.server.redis.ServerProperty;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.redis.RedisMessageRecieveEvent;
import net.metromc.spigot.util.Callback;
import net.metromc.spigot.util.D;
import net.metromc.spigot.util.UtilServer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class ServerRepository implements Listener {

    public static final String SERVER_INFO = "SERVER_INFO";
    public static final String SEND_SERVER_INFO = "SEND_SERVER_INFO";
    public static final String REQUEST_TO_CHANNEL = "REQUEST_TO_NETWORK";
    public static final String REQUEST_FROM_CHANNEL = "REQUEST_FROM_NETWORK";

    @Getter
    private List<Server> servers = Lists.newArrayList();
    private Map<String, List<Callback<String>>> callbacks = Maps.newHashMap();
    private List<ServerType> serverTypes = Lists.newArrayList();

    private final String name;
    private final ServerType type;
    private final MetroMC plugin;

    public ServerRepository(MetroMC plugin) {
        this.plugin = plugin;

        this.type = ServerType.fromCodeName(System.getProperty("type"));
        this.name = System.getProperty("name");

        plugin.getRedisHandler().registerChannel(SERVER_INFO, SEND_SERVER_INFO, REQUEST_FROM_CHANNEL, REQUEST_TO_CHANNEL);

        requestServerInfo(ServerProperty.SERVER_TYPE.name(), s -> {

            if (s.equals("[null]")) {
                D.d("String is null returning...");
                return;
            }

            D.d("GOT ANSWER FROM NETWORK " + s);

            JsonObject jsonObject = new JsonParser().parse(s).getAsJsonObject();
            JsonArray jsonArray = jsonObject.get("types").getAsJsonArray();

            jsonArray.forEach(jsonElement -> ServerType.fromJsonString(jsonElement.getAsString()));
        });
    }

    public Server getThisServer() {
        for (Server server : servers) {
            if (server.getName().equals(name)) {
                if (server.getHost().equals("-1")) {
                    String externalIP = UtilServer.getExternalIP();
                    server.setHost(externalIP);
                    plugin.getNetworkManager().registerServerInBungee(server.getName(), server.getHost(), server.getPort());
                }

                return server;
            }
        }

        Server server = new Server(name, UtilServer.getExternalIP(), Bukkit.getPort(), type, System.currentTimeMillis(), serialisePlayerList());
        servers.add(server);
        return server;
    }

    public void sendServerInfo(Server server) {
        if (server.getServerType() == null) {
            return;
        }

        plugin.getRedisHandler().publish(SEND_SERVER_INFO, server.toJsonString(System.currentTimeMillis(), serialisePlayerList()));
    }

    public void requestServerInfo(String property, Callback<String> callback) {
        callbacks.putIfAbsent(property, Lists.newArrayList());
        callbacks.get(property).add(callback);

        plugin.getRedisHandler().publish(REQUEST_TO_CHANNEL, property);
    }

    public List<String> getPlayers(boolean ignoreCase) {
        List<String> list = Lists.newArrayList();

        servers.forEach(server -> server.getOnlinePlayers().forEach(s -> {
            String name = s;
            if (ignoreCase) {
                name = name.toUpperCase();
            }
                list.add(name);
        }));

        return list;
    }

    public boolean isOnline(String player) {
        if (getPlayers(true).contains(player.toUpperCase())) {
            return true;
        }
        return false;
    }


    @EventHandler
    public void onRecieve(RedisMessageRecieveEvent e) {
        if (e.getChannel().equals(SERVER_INFO)) {
            List<Server> servers = fromJsonString(e.getMessage());

            this.servers = servers;
        }

        if (e.getChannel().equals(REQUEST_FROM_CHANNEL)) {
            String[] message = e.getMessage().split(" ");

            if (callbacks.containsKey(message[0])) {
                callbacks.get(message[0]).forEach(callback -> callback.result(message[1]));
                callbacks.get(message[0]).clear();
            }
        }
    }

    public static List<Server> fromJsonString(String string) {
        List<Server> list = Lists.newArrayList();
        JsonObject object = new JsonParser().parse(string).getAsJsonObject();

        JsonArray array = object.get("servers").getAsJsonArray();

        array.forEach(jsonElement -> list.add(Server.fromJsonString(jsonElement.getAsString())));

        return list;
    }

    public Set<String> serialisePlayerList() {
        Set<String> players = Sets.newHashSet();

        Bukkit.getOnlinePlayers().forEach(o -> {
            players.add(o.getName());
        });

        return players;
    }
}
