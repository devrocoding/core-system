package net.metromc.spigot.network.ui;

import net.metromc.network.server.data.Server;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.gui.Gui;
import net.metromc.spigot.gui.GuiElement;
import net.metromc.spigot.gui.context.GuiSize;
import net.metromc.spigot.util.ItemBuilder;
import net.metromc.spigot.util.UtilString;
import net.metromc.spigot.util.UtilTime;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class NetworkGui extends Gui {

    public NetworkGui(MetroMC plugin) {
        super(plugin, "Network Menu", GuiSize.FIVE_ROWS);

        for (Server server : plugin.getNetworkManager().getServerRepository().getServers()) {
            addElement(new GuiElement() {
                @Override
                public ItemStack getIcon(Player player) {
                    String players = "";

                    for (String name : server.getOnlinePlayers()) {
                        players += name + ", ";
                    }

                    return new ItemBuilder(Material.valueOf(server.getServerType().getMaterial())).setName(ChatColor.GREEN + server.getName()).setLore(ChatColor.GRAY + "Last response: " + UtilTime.getTimeSinceLastResponseInSeconds(server.getLastResponse()) + " seconds", ChatColor.GRAY + "Host: " + server.getHost(),
                            ChatColor.GRAY + "port: " + server.getPort(), ChatColor.GRAY + "Type: " + server.getServerType().getDisplayName(), ChatColor.GRAY + "Slots: " + server.getSlots(), ChatColor.GRAY + "Players: " + UtilString.split(players, 40)).build();
                }

                @Override
                public void click(Player player, ClickType clickType) {

                }
            });
        }
    }


}
