package net.metromc.spigot.network;

import lombok.Getter;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.data.Server;
import net.metromc.spigot.MetroMC;
import net.metromc.spigot.Module;
import net.metromc.spigot.network.command.CommandNetwork;
import net.metromc.spigot.util.D;
import net.metromc.spigot.util.UtilServer;

public class NetworkManager extends Module {

    @Getter
    private final ServerRepository serverRepository;

    public NetworkManager(MetroMC plugin) {
        super(plugin, "Network Manager");

        this.serverRepository = new ServerRepository(plugin);

        registerListener(serverRepository);
        registerCommand(new CommandNetwork(plugin));

        plugin.getRunnableManager().runTaskTimerAsynchronously("Serverdata pub", metroMC -> {
            Server server = serverRepository.getThisServer();

            serverRepository.sendServerInfo(serverRepository.getThisServer());
        }, 1 * 20, 2 * 20);
    }

    public void registerServerInBungee(String name, String host, int port) {
        getPlugin().getRedisHandler().publish(ServerDataRepository.REGISTER_SERVER_CHANNEL, name + " " + host + " " + port);
    }

    public void unregisterServerInBungee(String name) {
        getPlugin().getRedisHandler().publish(ServerDataRepository.UNREGISTER_SERVER_CHANNEL, name);
    }




}
