package net.metromc.network;

import java.io.File;
import java.net.URISyntaxException;
import lombok.Getter;
import net.metromc.network.command.CommandCenter;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.monitor.ServerMonitor;
import net.metromc.network.server.redis.RedisHandler;
import net.metromc.network.util.Logger;
import net.metromc.services.RedisService;
import org.fusesource.jansi.AnsiConsole;

public class Network {

    @Getter
    private static File FOLDER;
    @Getter
    private final static long STARTUP = System.currentTimeMillis();
    @Getter
    private static boolean autoManage = true;

    public static void main(String... args) {
        try {
            FOLDER = new File(Network.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getParentFile();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }

        AnsiConsole.systemInstall();
        Logger.getInstance();

        RedisService redisService = new RedisService(FOLDER);
        RedisHandler redisHandler = new RedisHandler(redisService);

        ServerMonitor serverMonitor = ServerMonitor.getInstance(ServerDataRepository.getInstance(), redisHandler);
        new CommandCenter(ServerDataRepository.getInstance(), serverMonitor, redisHandler);

        redisHandler.subscribe();

    }

}
