package net.metromc.network.process;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProcessRunner extends Thread {

    private ProcessBuilder processBuilder;
    private Process process;
    private GenericRunnable<Boolean> runnable;
    private boolean done, error;

    public ProcessRunner(String... args) {
        super("ProcessRunner " + args);
        processBuilder = new ProcessBuilder(args);
        done = false;
        error = false;
    }

    public void run() {
        try {
            process = processBuilder.start();
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.equals("255"))
                    error = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            done = true;
            if (runnable != null)
                runnable.run(error);
        }
    }

    public void start(GenericRunnable<Boolean> runnable) {
        super.start();
        this.runnable = runnable;
    }

    public int exitValue() throws IllegalStateException {
        if (process != null)
            return process.exitValue();
        throw new IllegalStateException("Process not started yet");
    }

    public boolean isDone() {
        return done;
    }

    public void abort() {
        if (!isDone())
            process.destroy();
    }
}
