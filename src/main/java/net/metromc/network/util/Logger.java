package net.metromc.network.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import jline.console.ConsoleReader;
import jline.console.CursorBuffer;
import net.metromc.network.Network;
import net.metromc.network.command.CommandCenter;

public class Logger {

	private static Logger instance;

	private static DateFormat DATE_FORMAT_LOG = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private static DateFormat DATE_FORMAT_CONSOLE = new SimpleDateFormat("HH:mm:ss");

	private java.util.logging.Logger logger = java.util.logging.Logger.getLogger("Network");
	private ConsoleReader console;
	private CursorBuffer stashed;
	private boolean command = false;

	public Logger() {
		try {
			console = new ConsoleReader();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		File logsFolder = new File(Network.getFOLDER().getPath() + File.separator + "logs");
		UtilFile.makeFolderIfNotExist(logsFolder);
		File logFile = new File(logsFolder.getPath() + File.separator + "log.log");
		if (!logFile.exists())
			try {
				logFile.createNewFile();
			} catch (IOException exception) {
				exception.printStackTrace();
			}
		try {
			FileHandler fileHandler = new FileHandler(logFile.getPath(), true);
			fileHandler.setFormatter(new Formatter() {
				public String format(LogRecord record) {
					return record.getMessage() + "\n";
				}
			});
			logger.addHandler(fileHandler);
			logger.setUseParentHandlers(false);
		} catch (SecurityException | IOException exception) {
			exception.printStackTrace();
		}
	}

	public static void debug(String message) {
		log("DEBUG", message);
	}

	public static void important(String message) {
		log("IMPORTANT", message);
	}

	public static void critical(String message) {
		log("CRITICAL", " !! " + message + " !!");
	}

	public static void error(String message) {
		log("ERROR", " !! " + message + " !!");
	}

	public static void log(String message) {
		log("INFO", message);
	}

	public static synchronized void log(String prefix, String message) {
		getInstance().processLog(prefix, message);
	}

	private synchronized void processLog(String prefix, String message) {
		stashTerminal();
		System.out
				.println(
						(prefix == null ? ""
								: "[" + DATE_FORMAT_CONSOLE.format(new Date())
										+ (prefix != null && prefix.length() > 0 ? " " + prefix : "") + "]: ")
								+ message);
		unstashTerminal();
		logger.info("[" + DATE_FORMAT_LOG.format(new Date())
				+ (prefix != null && prefix.length() > 0 ? " " + prefix : "") + "]: " + message);
	}

	private void stashTerminal() {
		stashed = console.getCursorBuffer().copy();
		try {
			console.getOutput().write("\u001b[1G\u001b[K");
			console.flush();
		} catch (IOException ignored) {
		}
	}

	private void unstashTerminal() {
		String prompt = console.getPrompt();
		if (command) {
			// Prevent two prompts being shown
			char[] characterArray = CommandCenter.PROMPT.toCharArray();
			for (int i = characterArray.length - 1; i >= 0; i--)
				prompt = UtilString.removeLastCharacter(prompt, characterArray[i]);
			// prompt = UtilString.replaceLast(prompt, CommandCenter.PROMPT,
			// "");
			// FOR SOME REASON THIS WASN'T WORKING?!
			command = false;
		}
		try {
			console.resetPromptLine(prompt, stashed.toString(), stashed.cursor);
		} catch (IOException ignored) {
		}
	}

	public ConsoleReader getConsoleReader() {
		return console;
	}

	public boolean hasCommand() {
		return command;
	}

	public void setCommand(boolean command) {
		this.command = command;
	}

	public static Logger getInstance() {
		if (instance == null)
			instance = new Logger();
		return instance;
	}

}