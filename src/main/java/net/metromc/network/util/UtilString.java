package net.metromc.network.util;

import java.util.HashMap;
import java.util.Map;

public class UtilString {

	public static String capitalise(String string) {
		char[] chars = string.toLowerCase().toCharArray();
		boolean found = false;
		for (int i = 0; i < chars.length; i++)
			if (!found && Character.isLetter(chars[i])) {
				chars[i] = Character.toUpperCase(chars[i]);
				found = true;
			} else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '\'')
				found = false;
		return String.valueOf(chars);
	}

	public static String capitaliseFirstCharacter(String string) {
		if (string == null || string.length() == 0)
			return string;
		return string.substring(0, 1).toUpperCase() + string.substring(1, string.length());
	}

	public static String removeLastCharacter(String string, char character) {
		char[] charArray = string.toCharArray();
		for (int i = charArray.length - 1; i >= 0; i--)
			if (charArray[i] == character) {
				charArray[i] = 0;
				break;
			}
		return new String(charArray);
	}

	public static String removeRecurringSpaces(String string) {
		return string.trim().replaceAll(" +", " ");
	}

	public static String replaceLast(String string, String regex, String replacement) {
		char[] stringArray = string.toCharArray();
		char[] regexArray = regex.toCharArray();
		char[] replacementArray = replacement.toCharArray();
		search: for (int i = stringArray.length - 1; i >= 0; i--) {
			int end = 0;
			for (int j = i; j < stringArray.length; j++) {
				if (j - i > regexArray.length - 1) {
					end = j;
					break;
				}
				if (stringArray[j] != regexArray[j - i])
					continue search;
			}
			for (int k = i; k < end; k++)
				stringArray[k] = (k - i > replacementArray.length - 1 ? 0 : replacementArray[k - i]);
			break;
		}
		return new String(stringArray);
	}

	public static Boolean getBoolean(String bool) {
		if (bool.equals("0") || bool.equalsIgnoreCase("false") || bool.equalsIgnoreCase("off")
				|| bool.equalsIgnoreCase("no") || bool.equalsIgnoreCase("disable") || bool.equalsIgnoreCase("disabled"))
			return false;
		if (bool.equals("1") || bool.equalsIgnoreCase("true") || bool.equalsIgnoreCase("on")
				|| bool.equalsIgnoreCase("yes") || bool.equalsIgnoreCase("enable") || bool.equalsIgnoreCase("enabled"))
			return true;
		return null;
	}

	public static String serialiseMap(Map<String, String> map) {
		String s = "";
		for (Map.Entry<String, String> entry : map.entrySet())
			s += entry.getKey() + ":" + entry.getValue() + ",";
		s = s.substring(0, Math.max(0, s.length() - 1));
		return s;
	}

	public static Map<String, String> deserialiseMap(String string) {
		Map<String, String> map = new HashMap<>();
		for (String s : string.split(",")) {
			if (s.split(":").length != 2)
				continue;
			map.put(s.split(":")[0], s.split(":")[1]);
		}
		return map;
	}

}