package net.metromc.network.command.commands;

import net.metromc.network.command.Command;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.data.Server;
import net.metromc.network.server.monitor.ServerMonitor;
import net.metromc.network.util.Logger;

public class CommandPlayers extends Command {

    private final ServerDataRepository serverDataRepository;

    public CommandPlayers(ServerDataRepository serverDataRepository) {
        super("players", "See all online players", "players");

        this.serverDataRepository = serverDataRepository;
    }

    @Override
    public void execute(String aliasUsed, String... args) {
        int players = 0;

        for (Server server : serverDataRepository.getServersAsList()) {
            players += server.getOnlinePlayers().size();
        }

        Logger.log("There are currently " + players + " online on the network");
    }
}
