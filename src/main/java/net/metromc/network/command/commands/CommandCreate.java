package net.metromc.network.command.commands;

import net.metromc.network.command.Command;
import net.metromc.network.server.data.ServerType;
import net.metromc.network.server.monitor.ServerMonitor;
import net.metromc.network.util.Logger;

public class CommandCreate extends Command {

    private final ServerMonitor serverMonitor;

    public CommandCreate(ServerMonitor serverMonitor) {
        super("Create", "Create a server", "create");
        setUsage("<type>");

        this.serverMonitor = serverMonitor;
    }

    @Override
    public void execute(String aliasUsed, String... args) {
        if (args.length == 0) {
            Logger.log(usage(aliasUsed));
            return;
        }
        ServerType serverType = ServerType.fromCodeName(args[0].toUpperCase());
        if (serverType == null) {
            Logger.log("servertype " + args[0] + " does not exist. Type servertypes for a list of all GameType(s).");
            return;
        }
        if (serverType.isStatic()) {
            Logger.log("Sorry, ServerType " + serverType.getDisplayName() + " is static, and new instances must be created manually.");
            return;
        }

        String name = serverType.getDisplayName() + "-" + serverMonitor.getNextAvailableIndex(serverType);

        int port = serverMonitor.getNextAvailablePort();
        if (port == -1) {
            Logger.log("No available ports to create servers on!");
            return;
        }

        Logger.log("Creating server " + name + "...");
        serverMonitor.createServer(name, port, serverType);
    }
}
