package net.metromc.network.command.commands;

import net.metromc.network.command.Command;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.data.Server;
import net.metromc.network.server.monitor.ServerMonitor;
import net.metromc.network.util.Logger;

public class CommandDestroy extends Command {

    private final ServerMonitor serverMonitor;

    public CommandDestroy(ServerMonitor serverMonitor) {
        super("destroy", "destroy a server", "destroy");
        setUsage("<server>");

        this.serverMonitor = serverMonitor;
    }

    @Override
    public void execute(String aliasUsed, String... args) {
        if (args.length == 0) {
            Logger.log(usage(aliasUsed));
            return;
        }

        Server server = ServerDataRepository.getInstance().getServer(args[0]);

        if (server == null) {
            Logger.log("We couldn't find a server named " + args[0]);
            return;
        }

        Logger.log("Destroying server " + server.getName() + "...");
        serverMonitor.destroyServer(server);
    }
}
