package net.metromc.network.command.commands;

import net.metromc.network.command.Command;
import net.metromc.network.util.Logger;

public class CommandStop extends Command {

    public CommandStop() {
        super("Stop", "Stop Networking", "stop", "quit", "exit");
    }

    @Override
    public void execute(String aliasUsed, String... args) {
        Logger.log("Stopping...");
        System.exit(0);
    }
}
