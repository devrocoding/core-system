package net.metromc.network.command.commands;

import net.metromc.network.command.Command;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.data.Server;
import net.metromc.network.util.Logger;

public class CommandServers extends Command {

    public CommandServers() {
        super("Servers", "See all servers", "servers");
    }

    @Override
    public void execute(String aliasUsed, String... args) {
        ServerDataRepository serverDataRepository = ServerDataRepository.getInstance();

        String msg = "servers (" + serverDataRepository.getServers().size() + ") ";
        for (Server server : serverDataRepository.getServersAsList()) {
            msg += server.getName() + ", ";
        }

        Logger.log(msg);
    }
}
