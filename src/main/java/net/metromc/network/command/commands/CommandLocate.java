package net.metromc.network.command.commands;

import net.metromc.network.command.Command;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.data.Server;
import net.metromc.network.util.Logger;

public class CommandLocate extends Command {

    private final ServerDataRepository serverDataRepository;

    public CommandLocate(ServerDataRepository serverDataRepository) {
        super("locate", "locate a player", "locate");
        setUsage("<player>");

        this.serverDataRepository = serverDataRepository;
    }

    @Override
    public void execute(String aliasUsed, String... args) {
        if (args.length == 0) {
            Logger.log(usage(aliasUsed));
            return;
        }
        Server targetServer = null;
        String name = args[0];

        for (Server server : serverDataRepository.getServersAsList()) {
            for (String cur : server.getOnlinePlayers()) {
                if (cur.toLowerCase().equals(name.toLowerCase())) {
                    targetServer = server;
                }
            }
        }

        if (targetServer == null) {
            Logger.log("We couldn't find a player online named " + name);
            return;
        }

        Logger.log(name + " is online on " + targetServer.getName());
    }
}
