package net.metromc.network.command;

import java.util.ArrayList;
import java.util.List;

public abstract class Command {

    private String name, description;
    private String[] aliases, usage;

    public Command(String name, String description, String... aliases) {
        this.name = name;
        this.description = description;
        this.aliases = aliases;
    }

    public abstract void execute(String aliasUsed, String... args);

    public void executeRaw(String aliasUsed, String raw, String... args) {
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getAliases() {
        List<String> aliases = new ArrayList<>();
        aliases.add(name);
        for (String alias : this.aliases)
            aliases.add(alias);
        return aliases;
    }

    public String[] getUsage() {
        if (usage == null)
            usage = new String[] {};
        return usage;
    }

    public String usage(String aliasUsed) {
        String s = aliasUsed.toLowerCase();
        if (usage != null)
            for (String usage : this.usage)
                s += " " + usage;
        return "Usage: " + s;
    }

    public void setUsage(String... usage) {
        this.usage = usage;
    }

}
