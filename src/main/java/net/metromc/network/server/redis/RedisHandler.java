package net.metromc.network.server.redis;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.List;
import lombok.Getter;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.data.Server;
import net.metromc.network.util.Logger;
import net.metromc.services.RedisService;
import net.metromc.services.redis.MultiChannelRedisModule;
import net.metromc.spigot.network.ServerRepository;
import net.metromc.spigot.util.D;

public class RedisHandler extends MultiChannelRedisModule {

    @Getter
    private final RedisService redisService;

    public RedisHandler(RedisService service) {
        super(service);

        this.redisService = service;
        registerChannel(ServerRepository.SEND_SERVER_INFO, ServerRepository.SERVER_INFO, ServerRepository.REQUEST_FROM_CHANNEL, ServerRepository.REQUEST_TO_CHANNEL, ServerDataRepository.REGISTER_SERVER_CHANNEL, ServerDataRepository.UNREGISTER_SERVER_CHANNEL);

        publisher().start();
    }


    @Override
    public void recieve(String channel, String message) {
        if (channel.equals(ServerRepository.SEND_SERVER_INFO)) {
            deserialiseServer(message);
        }

        if (channel.equals(ServerRepository.REQUEST_TO_CHANNEL)) {
            Logger.log("Recieved a request for the " + message + " property");
            ServerDataRepository serverDataRepository = ServerDataRepository.getInstance();

            String property = message.toUpperCase();
            ServerProperty serverProperty = ServerProperty.valueOf(property);

            if (serverProperty != null) {
                Logger.log("accepted consumer for property: " + message);
                serverProperty.getConsumer().accept(serverDataRepository, this);
            } else {
                Logger.error("Serverproperty " + property + " not found ignoring...");
            }
        }
    }

    private Thread publisher() {
        Thread thread = new Thread("ServerDataPub") {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

                try {
                    while (true) {
                        if (serialiseServers() != null) {
                            publish(ServerRepository.SERVER_INFO, serialiseServers());
                        }
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        return thread;
    }


    private void deserialiseServer(String message) {
        ServerDataRepository serverDataRepository = ServerDataRepository.getInstance();
        Server server = Server.fromJsonString(message);
        serverDataRepository.add(server);
    }

    private String serialiseServers() {
        List<Server> servers = ServerDataRepository.getInstance().getServersAsList();
        List<String> addedServers = Lists.newArrayList();
        if (servers.isEmpty()) {
            return null;
        }

        JsonObject jsonObject = new JsonObject();
        JsonArray jsonArray = new JsonArray();

        servers.forEach(server -> {
            if (addedServers.contains(server.getName())) {
                Logger.log("DUPLICATE " + server.getName());
                Logger.log("RETURNING FOR " + server.toJsonString());
                return;
            }

            addedServers.add(server.getName());
            jsonArray.add(server.toJsonString());
        });
        jsonObject.add("servers", jsonArray);

        return jsonObject.toString();
    }
}
