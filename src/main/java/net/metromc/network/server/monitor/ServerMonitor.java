package net.metromc.network.server.monitor;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import net.metromc.network.Network;
import net.metromc.network.process.ProcessRunner;
import net.metromc.network.server.ServerDataRepository;
import net.metromc.network.server.data.Server;
import net.metromc.network.server.data.ServerState;
import net.metromc.network.server.data.ServerType;
import net.metromc.network.server.redis.RedisHandler;
import net.metromc.network.util.Logger;
import net.metromc.network.util.UtilFile;
import net.metromc.spigot.util.D;
import net.minecraft.server.v1_12_R1.GameRules;

public class ServerMonitor extends Thread {

    private static ServerMonitor instance;
    private final ServerDataRepository dataRepository;
    private final RedisHandler redisHandler;

    private final double PERCENTAGE_FULL_TO_CREATE_NEW_INSTANCE = 70.0D;
    private final double MINIMUM_TPS = 17.0D;
    private final double MAX_RESPONSE_TIME_SECONDS = 40;

    private static final String ROOT_FOLDER = Network.getFOLDER().getPath();
    private static final String SCRIPT_FOLDER = ROOT_FOLDER + File.separator + "scripts";
    private static final String CREATE_SERVER_SCRIPT = "create-server.sh";
    private static final String DESTROY_SERVER_SCRIPT = "destroy-server.sh";
    private static final String REMOVE_SERVER_FOLDER_SCRIPT = "remove-server-folder.sh";
    private static final String START_STATIC_SERVER_SCRIPT = "start-static-server.sh";
    private static final String STOP_STATIC_SERVER_SCRIPT = "stop-static-server.sh";

    private final Set<ProcessRunner> processes = Sets.newHashSet();
    private Map<Integer, String> screens = Maps.newHashMap();
    private final Map<String, Long> serversBeingDestroyed = Maps.newHashMap();
    private final Map<String, Long> staticServersBeingStarted = Maps.newHashMap();

    public ServerMonitor(ServerDataRepository dataRepository, RedisHandler redisHandler) {
        this.dataRepository = dataRepository;
        this.redisHandler = redisHandler;
    }

    @Override
    public void run() {
        UtilFile.makeFolderIfNotExist(new File(ROOT_FOLDER + File.separator + "servers"));
        UtilFile.makeFolderIfNotExist(new File(ROOT_FOLDER + File.separator + "static-servers"));
        UtilFile.makeFolderIfNotExist(new File(ROOT_FOLDER + File.separator + "server-types-template"));

        UtilFile.copyFromResourcesFolder("scripts" + File.separator + CREATE_SERVER_SCRIPT, new File(SCRIPT_FOLDER));
        UtilFile.copyFromResourcesFolder("scripts" + File.separator + DESTROY_SERVER_SCRIPT, new File(SCRIPT_FOLDER));
        UtilFile.copyFromResourcesFolder("scripts" + File.separator + REMOVE_SERVER_FOLDER_SCRIPT,
                new File(SCRIPT_FOLDER));
        UtilFile.copyFromResourcesFolder("scripts" + File.separator + START_STATIC_SERVER_SCRIPT, new File(SCRIPT_FOLDER));
        UtilFile.copyFromResourcesFolder("scripts" + File.separator + STOP_STATIC_SERVER_SCRIPT, new File(SCRIPT_FOLDER));
        UtilFile.copyFromResourcesFolder("scripts" + File.separator + "manual-run-static-server-example.sh",
                new File(SCRIPT_FOLDER));
        UtilFile.copyFromResourcesFolder("scripts" + File.separator + "start-network.sh", new File(ROOT_FOLDER));

        try {
            Thread.sleep(5000);
        } catch (InterruptedException ignored) {
        }

        while (true) {
            if (System.currentTimeMillis() - Network.getSTARTUP() > TimeUnit.HOURS.toMillis(5)) {
                System.exit(0);
                return;
            }
            cleanupUnfinishedProcesses();
            destroyDeadServers();
            destroyEmptyServers();
            cleanupServersFolder();
            createInstancesToMeetMinimumRequirementForGameTypes();
            createGameTypeServersToHandleDemand();


            try {
                Thread.sleep(4000);
            } catch (InterruptedException exception) {
            }
        }
    }

    public void createServer(String name, int port, ServerType serverType) {
        Server server = new Server(name, "-1", port, serverType, System.currentTimeMillis(), new HashSet<>());
        server.setServerState(ServerState.OFFLINE);
        ServerDataRepository.getInstance().getServers().put(name.toUpperCase(), server);
        ProcessRunner processRunner = new ProcessRunner("/bin/sh", SCRIPT_FOLDER + File.separator + CREATE_SERVER_SCRIPT, name, serverType.getCodeName(), String.valueOf(port), String.valueOf(serverType.getMinRam()), String.valueOf(serverType.getMaxRam()));

        processRunner.start(error -> {
            if (error) {
                Logger.error("Error creating server " + server.getName());
            } else {
                Logger.important("Created server " + server.getName());
            }
        });

        try {
            processRunner.join(100);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        if (!processRunner.isDone()) {
            processes.add(processRunner);
        }
    }

    private void destroyDeadServers() {
        if (System.currentTimeMillis() - Network.getSTARTUP() < MAX_RESPONSE_TIME_SECONDS * 1000) {
            return;
        }

        for (Server server : ServerDataRepository.getInstance().getServersAsList()) {
            if (server.getTimeSinceLastPingInSeconds() > 30 && server.getUptimeInSeconds() > 60) {
                Logger.important("Server " + server.getName() + " is dead, destroying it");
                destroyServer(server);
            }
        }
    }

    private void destroyEmptyServers() {
        if (!Network.isAutoManage()) {
            return;
        }
        for (Server server : ServerDataRepository.getInstance().getServersAsList()) {
            if (server.getOnlinePlayers().isEmpty() && server.getNumber() != 1 && ServerDataRepository.getInstance().getServers(server.getServerType()).size() > server.getServerType().getMinInstances() && !server.getServerType().isStatic()) {
                Logger.log("Server " + server.getName() + " is empty, and GameType " + server.getServerType().toString() + " has more than one instance, destroying " + server.getName());
                destroyServer(server);
            }
        }
    }

    private void createInstancesToMeetMinimumRequirementForGameTypes() {
        if (!Network.isAutoManage()) {
            return;
        }

        for (ServerType serverType : ServerType.values()) {
            List<Server> servers = dataRepository.getServers(serverType);

            if (serverType.getMinInstances() > 0) {
                int minServers = serverType.getMinInstances();
                int current = servers.size();

                if (minServers < current) {
                    String name = serverType.getDisplayName() + "-" + getNextAvailableIndex(serverType);
                    int port = getNextAvailablePort();

                    if (port == -1) {
                        for (int i = 0; i < 5; i++) {
                            Logger.critical("NO AVAILABLE PORTS, CAN NOT CREATE NEW INSTANCES");
                        }
                        return;
                    }

                    if (name.toLowerCase().equals("hub-1")) {
                        port = 100;
                    }

                    Logger.important("Not enough " + serverType.getDisplayName() + " servers creating " + name);
                    createServer(name, port, serverType);
                    return;
                }
            }
        }
    }

    private void createGameTypeServersToHandleDemand() {
        if (!Network.isAutoManage()) {
            return;
        }

        for (ServerType serverType : ServerType.values()) {
            if (!serverType.isStatic() && (serverType.getMinInstances() < ServerDataRepository.getInstance().getServers(serverType).size())) {
                int players = 0, slots = 0;

                for (Server server : ServerDataRepository.getInstance().getServers(serverType)) {
                    players += server.getOnlinePlayers().size();
                    slots += server.getSlots();
                }

                double percentage = (Double.valueOf(players) / Double.valueOf(slots)) * 100;
                if (percentage > PERCENTAGE_FULL_TO_CREATE_NEW_INSTANCE) {
                    int port = getNextAvailablePort();
                    if (port == -1) {
                        for (int i = 0; i < 5; i++) {
                            Logger.critical("NO AVAILABLE PORTS, CAN NOT CREATE NEW INSTANCES");
                        }
                        return;
                    }

                    Logger.important("Creating new server to handle the demmand for " + serverType.getDisplayName());
                    createServer(serverType.getDisplayName() + "-" + getNextAvailableIndex(serverType), getNextAvailablePort(), serverType);
                    return;
                }
            }
        }
    }

    private void cleanupServersFolder() {
        if (!Network.isAutoManage()) {
            return;
        }

        File serversFolder = new File(ROOT_FOLDER + File.separator + "servers");
        List<String> servers = Lists.newArrayList();
        for (File file : serversFolder.listFiles()) {
            if (file.isFile())
                file.delete();
            else if (file.isDirectory())
                servers.add(file.getName());
        }
        for (String folder : servers)
            if (ServerDataRepository.getInstance().getServer(folder) == null && (!serversBeingDestroyed.containsKey(folder) || System.currentTimeMillis() - serversBeingDestroyed.get(folder) > 3000)) {
                Logger.important("Found server folder " + serversFolder.getPath() + File.separator + folder + " but no screen process exists. Removing folder.");

                ProcessRunner processRunner = new ProcessRunner("/bin/sh", SCRIPT_FOLDER + File.separator + REMOVE_SERVER_FOLDER_SCRIPT, folder);
                processRunner.start((Boolean error) -> {
                    if (error)
                        Logger.error("Error removing server folder " + folder);
                    else
                        Logger.important("Removed server folder " + folder);
                });

                try {
                    processRunner.join(100);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
                if (!processRunner.isDone()) {
                    processes.add(processRunner);
                }
            }
    }

    public void destroyServer(Server server) {
        ServerDataRepository.getInstance().getServers().remove(server.getName().toUpperCase());
        serversBeingDestroyed.put(server.getName(), System.currentTimeMillis());
        unregisterServerInBungee(server.getName());

        ProcessRunner processRunner = new ProcessRunner("/bin/sh", SCRIPT_FOLDER + File.separator + DESTROY_SERVER_SCRIPT, server.getName(), String.valueOf(server.getPort()));
        processRunner.start(error -> {
            if (error) {
                Logger.error("Error while destroying server " + server.getName());
            } else {
                unregisterServerInBungee(server.getName());
                Logger.important("Destroyed server " + server.getName());
            }
        });

        try {
            processRunner.join(100);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        if (!processRunner.isDone()) {
            processes.add(processRunner);
        }

        if (server.getName().toLowerCase().equals("hub-1")) {
            createServer("Hub-1", 100, ServerType.HUB);
        }
    }

    private void cleanupUnfinishedProcesses() {
        int processWaits = 0;
        while (processes.size() > 0) {
            for (Iterator<ProcessRunner> iterator = processes.iterator(); iterator.hasNext(); ) {
                ProcessRunner processRunner = iterator.next();
                try {
                    processRunner.join(100);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

                if (processRunner.isDone()) {
                    iterator.remove();
                }
            }

            if (processes.size() > 0) {
                try {
                    Logger.log("Sleeping while processes run...");
                    Thread.sleep(6000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            if (processWaits >= 10) {
                Logger.log("Killing stale processes");
                for (Iterator<ProcessRunner> iterator = processes.iterator(); iterator.hasNext(); ) {
                    iterator.next().abort();
                    iterator.remove();
                }
            }
            processWaits++;
        }
    }

    public int getNextAvailableIndex(ServerType serverType) {
        List<Server> servers = ServerDataRepository.getInstance().getServersAsList(serverType);
        int max = 1;
        for (Server server : servers) {
            max = Math.max(max, server.getNumber());
        }

        int available = max + 1;
        search:
        for (int i = 1; i <= max; i++) {
            for (Server server : servers) {
                if (server.getNumber() == i) {
                    continue search;
                }
            }

            available = i;
            break;
        }
        return available;
    }

    public int getNextAvailablePort() {
        for (int port = 101; port < 300; port++)
            if (ServerDataRepository.getInstance().getServer(port) == null) {
                return port;
            }
        return -1;
    }

    public void unregisterServerInBungee(String name) {
        redisHandler.publish(ServerDataRepository.UNREGISTER_SERVER_CHANNEL, name);
    }


    public static ServerMonitor getInstance(ServerDataRepository dataRepository, RedisHandler redisHandler) {
        if (instance == null) {
            instance = new ServerMonitor(dataRepository, redisHandler);
            instance.start();
        }

        return instance;
    }
}
