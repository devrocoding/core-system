package net.metromc.network.server.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import net.metromc.network.util.Logger;
import net.metromc.spigot.util.D;
import net.metromc.spigot.util.UtilJson;

public class ServerType {

    private static final List<ServerType> serverTypes = Lists.newArrayList();

    public static ServerType HUB = new ServerType("HUB", "Hub", "BED", 512, 1024) {
        {
            getData().put("minInstances", String.valueOf(2));
            getData().put("maxplayers", String.valueOf(100));
        }
    }.create(),

    METRO = new ServerType("METRO", "Metro", "DIAMOND_SWORD", 1024, 2048) {
        {
            getData().put("maxPlayers", String.valueOf(125));
        }
    }.create();

    private final String codeName;
    private final String displayName;
    private final String material;
    private int minRam, maxRam;
    private Map<String, String> data;


    public ServerType(String codeName, String displayName, String material, int minRam, int maxRam) {
        this.codeName = codeName;
        this.displayName = displayName;
        this.material = material;
        this.minRam = minRam;
        this.maxRam = maxRam;
        this.data = Maps.newHashMap();
    }

    public ServerType create() {
        serverTypes.add(this);
        return this;
    }

    public int getMinInstances() {
        if (!data.containsKey("minInstances")) {
            return 1;
        } else {
            return Integer.valueOf(data.get("minInstances"));
        }
    }

    public boolean isStatic() {
        if (!data.containsKey("static")) {
            return false;
        }

        return true;
    }

    public static ServerType fromCodeName(String codeName) {
        for (ServerType serverType : serverTypes) {
            if (serverType.codeName.equals(codeName)) {
                return serverType;
            }
        }
        return null;
    }

    public String toJsonString() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("codeName", codeName);
        jsonObject.addProperty("displayName", displayName);
        jsonObject.addProperty("material", material);
        jsonObject.addProperty("minRam", minRam);
        jsonObject.addProperty("maxRam", maxRam);

        return jsonObject.toString();
    }

    public static void fromJsonString(String msg) {
        JsonObject jsonObject = new JsonParser().parse(msg).getAsJsonObject();
        String codeName = UtilJson.getString(jsonObject, "codeName");
        String displayName = UtilJson.getString(jsonObject, "codeName");
        String material = UtilJson.getString(jsonObject, "codeName");
        int minRam = UtilJson.getInt(jsonObject, "minRam");
        int maxRam = UtilJson.getInt(jsonObject, "maxRam");

        ServerType serverType = new ServerType(codeName, displayName, material, minRam, maxRam);

        if (serverTypes.stream().filter(serverType1 -> serverType1.codeName.equals(serverType.codeName)).findFirst() == null) {
            serverTypes.add(serverType);
        }
    }

    public static List<ServerType> values() {
        return new ArrayList<>(serverTypes);
    }

    public String getCodeName() {
        return codeName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getMaterial() {
        return material;
    }

    public int getMinRam() {
        return minRam;
    }

    public int getMaxRam() {
        return maxRam;
    }

    public Map<String, String> getData() {
        return data;
    }
}
